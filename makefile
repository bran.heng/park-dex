# Referencing GeoJob (Fall 2022) makefile

clean:
	rm -f *.tmp

# get git status
status :
	make clean
	@echo
	git branch
	git remote -v
	git status

# download files from the repository
pull :
	make clean
	@echo
	git pull
	git status

# run unit tests
tests :
	echo "Running unit tests for back-end..."
	python3 backend/unit_tests.py

# build frontend
build-frontend :
	docker build -t front-end frontend/

# build backend
build-backend :
	docker build -t back-end backend/

all: