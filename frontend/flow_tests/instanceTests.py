import unittest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By

# Copied from https://gitlab.com/sarthaksirotiya/cs373-idb/-/blob/main/front-end/flow_tests/instanceTests.py

URL = "https://www.park-dex.me/"


class Test(unittest.TestCase):
    @classmethod
    def setUpClass(self) -> None:
        options = webdriver.ChromeOptions()
        options.add_experimental_option("excludeSwitches", ["enable-logging"])
        options.add_argument("--headless")
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-dev-shm-usage")
        chrome_prefs = {}
        options.experimental_options["prefs"] = chrome_prefs
        # Disable images
        chrome_prefs["profile.default_content_settings"] = {"images": 2}
        self.driver = webdriver.Chrome(
            options=options, service=Service(ChromeDriverManager().install())
        )
        self.driver.get(URL)

    @classmethod
    def tearDownClass(self):
        self.driver.quit()

    def test_animals(self):
        self.driver.get(URL + "animals")
        self.assertEqual(self.driver.current_url, URL + "animals/")

    def test_parks(self):
        self.driver.get(URL + "parks")
        self.assertEqual(self.driver.current_url, URL + "parks/")

    def test_states(self):
        self.driver.get(URL + "states")
        self.assertEqual(self.driver.current_url, URL + "states/")


if __name__ == "__main__":
    unittest.main()
