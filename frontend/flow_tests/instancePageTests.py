import unittest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

URL = "https://www.park-dex.me/"


class Test(unittest.TestCase):
    @classmethod
    def setUpClass(self) -> None:
        options = webdriver.ChromeOptions()
        options.add_experimental_option("excludeSwitches", ["enable-logging"])
        options.add_argument("--headless")
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-dev-shm-usage")
        options.add_argument("--window-size=1920,1080")
        chrome_prefs = {}
        options.experimental_options["prefs"] = chrome_prefs
        # Disable images
        chrome_prefs["profile.default_content_settings"] = {"images": 2}
        self.driver = webdriver.Chrome(
            options=options, service=Service(ChromeDriverManager().install())
        )
        self.driver.get(URL)

    @classmethod
    def tearDownClass(self):
        self.driver.quit()

    def test_animal_instance(self):
        self.driver.get(URL + "animals")
        try:
            WebDriverWait(self.driver, 50, 1).until(
                EC.element_to_be_clickable(
                    (By.XPATH, '//*[@id="root"]/div/div/div/div[4]/div[1]/div/a')
                )
            )
            element = self.driver.find_element(
                By.XPATH, '//*[@id="root"]/div/div/div/div[4]/div[1]/div/a'
            )
            element.click()
        except Exception as ex:
            print("Couldn't find Animal button: " + str(ex))

        self.assertEqual(self.driver.current_url, URL + "animals/ablautus_schlingeri")

if __name__ == "__main__":
    unittest.main()
