import os
from sys import platform

# Copied code from https://gitlab.com/sarthaksirotiya/cs373-idb/-/blob/main/front-end/guitests.py
# Edited code to implement the tests that we want

if __name__ == "__main__":
    # Use chromedriver based on OS
    if platform == "win32":
        PATH = "./front-end/flow_tests/chromedriver.exe"
    elif platform == "linux":
        PATH = "./front-end/flow_tests/chromedriver_linux"
    else:
        print("Unsupported OS")
        exit(-1)

    # Run all of the gui tests
    os.system("python3 ./flow_tests/instancePageTests.py")
    os.system("python3 ./flow_tests/navbarTests.py")
    os.system("python3 ./flow_tests/instanceTests.py")
