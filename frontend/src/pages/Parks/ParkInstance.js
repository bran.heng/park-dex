import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Loading from "../../components/Loading";
import ParkDetails from "../../components/Parks/ParkDetails.js";
import { getPark } from "../../utils/api";

const ParkInstance = () => {
  const { park_id } = useParams();

  const [loading, setLoading] = useState(true);
  const [park, setPark] = useState(null);

  useEffect(() => {
    const fetchPark = async () => {
      const park = await getPark(park_id);
      setPark(park);
      setLoading(false);
    };
    setLoading(true);
    fetchPark();
  }, [park_id]);

  return (
    <React.Fragment>
      {loading && <Loading />}
      {!loading && <ParkDetails park={park} showLatLong={true} />}
    </React.Fragment>
  );
};

export default ParkInstance;
