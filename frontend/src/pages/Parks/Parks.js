import React, { useState, useEffect } from "react";
import "../../styles/Parks.css";
import {
  get_parks_length,
  fetch_parks,
  search_model,
} from "../../utils/pagination.js";
import Loading from "../../components/Loading";
import PaginationIndicator from "../../components/PaginationIndicator";
import ParkCard from "../../components/Parks/ParkCard.js";
import SearchBar from "../../components/SearchBar";
import CustomDropDown from "../../components/CustomDropDown";

function Parks() {
  const [currPage, setCurrPage] = useState(1);
  const [pageLoading, setPageLoading] = useState(true);
  const [loading, setLoading] = useState(true);
  const [parks, setParks] = useState(null);
  const [numPages, setNumPages] = useState(null);
  const [highlight, setHighlight] = useState("");
  const [reload, setReload] = useState(0);
  const [sort, setSort] = useState(null);
  const [order, setOrder] = useState(null);
  const [activitiesFilter, setActivitiesFilter] = useState(null);
  const [statesFilter, setStatesFilter] = useState(null);
  const [perPage, setPerPage] = useState(20);

  const handleSearch = async (search_term) => {
    setLoading(true);
    const search_data = await search_model(search_term, "parks");
    setParks(search_data);
    setHighlight(search_term);
    setLoading(false);
  };

  const handleSubmit = () => {
    setReload(reload + 1);
  };

  function handleSort(field) {
    if (field === "Sort") {
      setSort(null);
    } else {
      setSort(field.toLowerCase());
    }
  }

  function handleOrder(order) {
    let newOrder = null;
    switch (order) {
      case "Ascending":
        newOrder = "asc";
        break;
      case "Descending":
        newOrder = "desc";
        break;
      default:
        newOrder = null;
    }
    setOrder(newOrder);
  }

  function handleActivitiesFilter(activity) {
    if (activity === "Activity") {
      setActivitiesFilter(null);
    } else {
      setActivitiesFilter(activity.toLowerCase());
    }
  }

  function handleStatesFilter(state) {
    if (state === "State") {
      setStatesFilter(null);
    } else {
      setStatesFilter(state);
    }
  }

  function handlePerPage(perPageText) {
    if (perPageText === "Results Per Page") {
      setPerPage(20);
    } else {
      setPerPage(parseInt(perPageText));
    }
  }

  useEffect(() => {
    const load_num_parks = async () => {
      const num_parks = await get_parks_length();
      setNumPages(Math.ceil(num_parks / perPage));
      setPageLoading(false);
    };
    setLoading(true);
    setPageLoading(true);
    load_num_parks();
  }, []);

  useEffect(() => {
    const load_parks = async () => {
      const parks = await fetch_parks(
        currPage,
        sort,
        order,
        activitiesFilter,
        statesFilter,
        perPage
      );
      setParks(parks);
      setLoading(false);
    };

    if (!pageLoading) {
      setLoading(true);
      load_parks();
    }
  }, [currPage, pageLoading, reload]);

  return (
    <React.Fragment>
      <div className="mainContainer">
        <h1 className="titleText">Parks</h1>
        <SearchBar model="Parks" handleSearch={handleSearch} />

        <div className="dropdownContainer">
          <CustomDropDown
            title="Results Per Page"
            items={["20", "40", "100"]}
            func={handlePerPage}
          />

          <CustomDropDown
            title="Sort"
            items={["Sort", "Biodiversity"]}
            func={handleSort}
          />
          <CustomDropDown
            title="Order"
            items={["Ascending", "Descending"]}
            func={handleOrder}
          />
          <CustomDropDown
            title="Activity"
            items={[
              "Activity",
              "Astronomy",
              "Stargazing",
              "Biking",
              "Camping",
              "Group Camping",
              "Fishing",
              "Food",
              "Guided Tours",
              "Hiking",
              "Front-Country Hiking",
              "Horse Trekking",
              "Paddling",
              "Junior Ranger Program",
              "Wildlife Watching",
              "Birdwatching",
              "Park Film",
              "Shopping",
              "Bookstore and Park Store",
              "Backcountry Camping",
              "Car or Front Country Camping",
              "Backcountry Hiking",
              "Canoe or Kayak Camping",
              "Museum Exhibits",
            ]}
            func={handleActivitiesFilter}
            scroll
          />
          <CustomDropDown
            title="State"
            items={[
              "State",
              "AL",
              "AK",
              "AZ",
              "AR",
              "CA",
              "CO",
              "CT",
              "DE",
              "DC",
              "FL",
              "GA",
              "HI",
              "ID",
              "IL",
              "IN",
              "IA",
              "KS",
              "KY",
              "LA",
              "ME",
              "MD",
              "MA",
              "MI",
              "MN",
              "MS",
              "MO",
              "MT",
              "NE",
              "NV",
              "NH",
              "NJ",
              "NM",
              "NY",
              "NC",
              "ND",
              "OH",
              "OK",
              "OR",
              "PA",
              "RI",
              "SC",
              "SD",
              "TN",
              "TX",
              "UT",
              "VT",
              "VA",
              "WA",
              "WV",
              "WI",
              "WY",
              "AS",
              "GU",
              "MP",
              "PR",
              "VI",
            ]}
            func={handleStatesFilter}
            scroll
          />
        </div>

        <button className="filter-submit-button" onClick={handleSubmit}>
          Submit
        </button>

        {loading && <Loading />}
        {!loading && (
          <React.Fragment>
            <div className="pagination-ind">
              <PaginationIndicator
                currPage={currPage}
                setCurrPage={setCurrPage}
                numPages={numPages}
              />
            </div>

            <div className="cardBox">
              {parks !== null &&
                parks.map((park, index) => {
                  return (
                    <ParkCard key={index} park={park} highlight={highlight} />
                  );
                })}
            </div>
            <PaginationIndicator
              currPage={currPage}
              setCurrPage={setCurrPage}
              numPages={numPages}
            />
          </React.Fragment>
        )}
      </div>
    </React.Fragment>
  );
}

export default Parks;
