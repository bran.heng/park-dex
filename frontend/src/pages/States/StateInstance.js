import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Loading from "../../components/Loading";
import StateDetails from "../../components/States/StateDetails";
import { getState } from "../../utils/api";

const StateInstance = () => {
  const { state_abbr } = useParams();

  const [loading, setLoading] = useState(true);
  const [state, setState] = useState(null);

  useEffect(() => {
    const fetchState = async () => {
      const state = await getState(state_abbr);
      setState(state);
      setLoading(false);
    };
    setLoading(true);
    fetchState();
  }, [state_abbr]);

  return (
    <React.Fragment>
      {loading && <Loading />}
      {!loading && <StateDetails state={state} />}
    </React.Fragment>
  );
};

export default StateInstance;
