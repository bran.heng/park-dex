import React, { useState, useEffect } from "react";
import "../../styles/Parks.css";
import {
  get_states_length,
  fetch_states,
  search_model,
} from "../../utils/pagination.js";
import Loading from "../../components/Loading";
import PaginationIndicator from "../../components/PaginationIndicator";
import StateCard from "../../components/States/StateCard.js";
import SearchBar from "../../components/SearchBar";
import CustomDropDown from "../../components/CustomDropDown";

function States() {
  const [currPage, setCurrPage] = useState(1);
  const [statePagesLoading, setStatePagesLoading] = useState(true);
  const [loading, setLoading] = useState(true);
  const [states, setStates] = useState(null);
  const [numPages, setNumPages] = useState(null);
  const [highlight, setHighlight] = useState("");
  const [reload, setReload] = useState(0);
  const [sort, setSort] = useState(null);
  const [order, setOrder] = useState(null);
  const [perPage, setPerPage] = useState(20);

  const handleSearch = async (search_term) => {
    setLoading(true);
    const search_data = await search_model(search_term, "states");
    setStates(search_data);
    setHighlight(search_term);
    setLoading(false);
  };

  const handleSubmit = () => {
    setReload(reload + 1);
  };

  function handleSort(field) {
    let newSort = null;
    switch (field) {
      case "Name":
        newSort = "name";
        break;
      case "Land Area":
        newSort = "land_area";
        break;
      case "Water Area":
        newSort = "water_area";
        break;
      case "Biodiversity":
        newSort = "biodiversity";
        break;
      case "Number of Parks":
        newSort = "number_of_parks";
        break;
      default:
        newSort = null;
    }
    setSort(newSort);
  }

  function handleOrder(order) {
    let newOrder = null;
    switch (order) {
      case "Ascending":
        newOrder = "asc";
        break;
      case "Descending":
        newOrder = "desc";
        break;
      default:
        newOrder = null;
    }
    setOrder(newOrder);
  }

  function handlePerPage(perPageText) {
    if (perPageText === "Results Per Page") {
      setPerPage(20);
    } else {
      setPerPage(parseInt(perPageText));
    }
  }

  useEffect(() => {
    const load_num_states = async () => {
      const num_states = await get_states_length();
      setNumPages(Math.ceil(num_states / perPage));
      setStatePagesLoading(false);
    };
    setStatePagesLoading(true);
    setLoading(true);
    load_num_states();
  }, []);

  useEffect(() => {
    const load_states = async () => {
      if (!statePagesLoading) {
        const states = await fetch_states(currPage, sort, order, perPage);
        // console.log(states);
        setStates(states);
        setLoading(false);
      }
    };
    setLoading(true);
    load_states();
  }, [currPage, statePagesLoading, reload]);

  return (
    <React.Fragment>
      <div className="mainContainer">
        <h1 className="titleText">States</h1>
        <SearchBar model="States" handleSearch={handleSearch} />

        <div className="dropdownContainer">
          <CustomDropDown
            title="Results Per Page"
            items={["20", "40", "100"]}
            func={handlePerPage}
          />

          <CustomDropDown
            title="Sort"
            items={[
              "Sort",
              "Name",
              "Land Area",
              "Water Area",
              "Biodiversity",
              "Number of Parks",
            ]}
            func={handleSort}
          />

          <CustomDropDown
            title="Order"
            items={["Ascending", "Descending"]}
            func={handleOrder}
          />
        </div>

        <button className="filter-submit-button" onClick={handleSubmit}>
          Submit
        </button>

        {loading && <Loading />}
        {!loading && (
          <React.Fragment>
            <div className="pagination-ind">
              <PaginationIndicator
                currPage={currPage}
                setCurrPage={setCurrPage}
                numPages={numPages}
              />
            </div>

            <div className="cardBox">
              {states !== null &&
                states.map((state, index) => {
                  return (
                    <StateCard key={index} data={state} highlight={highlight} />
                  );
                })}
            </div>
            <PaginationIndicator
              currPage={currPage}
              setCurrPage={setCurrPage}
              numPages={numPages}
            />
          </React.Fragment>
        )}
      </div>
    </React.Fragment>
  );
}

export default States;
