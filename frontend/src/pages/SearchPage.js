import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { searchModels } from "../utils/api.js";
import "../styles/SearchPage.css";
import Loading from "../components/Loading";
import AnimalBox from "../components/Animals/AnimalBox.js";
import StateBox from "../components/States/StateBox.js";
import ParkBox from "../components/Parks/ParkBox.js";

const SearchPage = () => {
  const [activeTab, setActiveTab] = useState("animals");
  const [animals, setAnimals] = useState(null);
  const [parks, setParks] = useState(null);
  const [states, setStates] = useState(null);
  const [loading, setLoading] = useState(true);
  const { search_term } = useParams();

  useEffect(() => {
    const fetchSearchResults = async () => {
      const data = await searchModels(search_term);
      setAnimals(data["animals"]);
      setParks(data["parks"]);
      setStates(data["states"]);
      setLoading(false);
    };
    fetchSearchResults();
  }, [search_term]);
  console.log(animals, parks, states);

  const handleTabClick = (tabName) => {
    setActiveTab(tabName);
  };

  const renderTabContent = () => {
    switch (activeTab) {
      case "animals":
        return <AnimalBox animals={animals} highlight={search_term} />;
      case "states":
        return <StateBox states={states} highlight={search_term} />;
      case "parks":
        return <ParkBox parks={parks} highlight={search_term} />;
      default:
        return null;
    }
  };

  return (
    <React.Fragment>
      {loading && <Loading />}
      {!loading && (
        <div className="tabbed-layout">
          <div className="tab-buttons">
            <button
              className={activeTab === "animals" ? "active" : ""}
              onClick={() => handleTabClick("animals")}
            >
              Animals
            </button>
            <button
              className={activeTab === "states" ? "active" : ""}
              onClick={() => handleTabClick("states")}
            >
              States
            </button>
            <button
              className={activeTab === "parks" ? "active" : ""}
              onClick={() => handleTabClick("parks")}
            >
              Parks
            </button>
          </div>
          <div className="tab-content">{renderTabContent()}</div>
        </div>
      )}
    </React.Fragment>
  );
};

export default SearchPage;
