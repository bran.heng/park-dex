import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Loading from "../../components/Loading";
import AnimalDetails from "../../components/Animals/AnimalDetails.js";
import { getAnimal } from "../../utils/api";

const AnimalInstance = () => {
  const { animal_id } = useParams();

  const [loading, setLoading] = useState(true);
  const [animal, setAnimal] = useState(null);

  useEffect(() => {
    const fetchAnimal = async () => {
      const animal = await getAnimal(animal_id);
      // console.log(animal)
      setAnimal(animal);
      setLoading(false);
    };
    setLoading(true);
    fetchAnimal();
  }, [animal_id]);

  return (
    <React.Fragment>
      {loading && <Loading />}
      {!loading && <AnimalDetails animal={animal} />}
    </React.Fragment>
  );
};

export default AnimalInstance;
