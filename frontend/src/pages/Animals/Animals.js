import React, { useState, useEffect } from "react";
import "../../styles/Parks.css";
import {
  get_animals_length,
  fetch_animals,
  search_model,
} from "../../utils/pagination.js";
import Loading from "../../components/Loading";
import PaginationIndicator from "../../components/PaginationIndicator";
import AnimalCard from "../../components/Animals/AnimalCard.js";
import SearchBar from "../../components/SearchBar";
import CustomDropDown from "../../components/CustomDropDown";

function Animals() {
  const [currPage, setCurrPage] = useState(1);
  const [loading, setLoading] = useState(true);
  const [loadingPage, setLoadingPage] = useState(true);
  const [animals, setAnimals] = useState(null);
  const [numPages, setNumPages] = useState(null);
  const [highlight, setHighlight] = useState("");
  const [reload, setReload] = useState(0);
  const [sort, setSort] = useState(null);
  const [order, setOrder] = useState(null);
  const [groupFilter, setGroupFilter] = useState(null);
  const [statesFilter, setStatesFilter] = useState(null);
  const [endangeredFilter, setEndangeredFilter] = useState(null);
  const [perPage, setPerPage] = useState(20);

  const handleSearch = async (search_term) => {
    setLoading(true);
    const search_data = await search_model(search_term, "animals");
    console.log(search_data);
    setAnimals(search_data);
    setHighlight(search_term);
    setLoading(false);
  };

  const lower_underlines = (input_string) => {
    return input_string.toLowerCase().replace(/ /g, "_");
  };

  const handleSubmit = () => {
    setReload(reload + 1);
  };

  function handleSort(field) {
    if (field === "Sort") {
      setSort(null);
    } else {
      setSort(lower_underlines(field));
    }
  }

  function handleOrder(order) {
    let newOrder = null;
    switch (order) {
      case "Ascending":
        newOrder = "asc";
        break;
      case "Descending":
        newOrder = "desc";
        break;
      default:
        newOrder = null;
    }
    setOrder(newOrder);
  }

  function handleGroupFilter(activity) {
    if (activity === "Group") {
      setGroupFilter(null);
    } else {
      setGroupFilter(activity);
    }
  }

  function handleStatesFilter(state) {
    if (state === "State") {
      setStatesFilter(null);
    } else {
      setStatesFilter(state);
    }
  }

  function handleEndangeredFilter(endangeredStatus) {
    if (endangeredStatus === "Endangered Status") {
      setEndangeredFilter(null);
    } else {
      setEndangeredFilter(endangeredStatus);
    }
  }

  function handlePerPage(perPageText) {
    if (perPageText === "Results Per Page") {
      setPerPage(20);
    } else {
      setPerPage(parseInt(perPageText));
    }
  }

  useEffect(() => {
    const load_num_animals = async () => {
      const num_animals = await get_animals_length();
      setNumPages(Math.ceil(num_animals / perPage));
      setLoadingPage(false);
    };
    setLoadingPage(true);
    setLoading(true);
    load_num_animals();
  }, []);

  useEffect(() => {
    const load_parks = async () => {
      if (!loadingPage) {
        const animals = await fetch_animals(
          currPage,
          sort,
          order,
          groupFilter,
          statesFilter,
          endangeredFilter,
          perPage
        );
        // console.log(animals);
        setAnimals(animals);
        setLoading(false);
      }
    };
    setLoading(true);
    load_parks();
  }, [loadingPage, currPage, reload]);

  return (
    <React.Fragment>
      <div className="mainContainer">
        <h1 className="titleText">Animals</h1>
        <SearchBar model="Animals" handleSearch={handleSearch} />
        <div className="dropdownContainer">
          <CustomDropDown
            title="Results Per Page"
            items={["20", "40", "100"]}
            func={handlePerPage}
          />

          <CustomDropDown
            title="Sort"
            items={["Sort", "Name", "Scientific Name"]}
            func={handleSort}
          />

          <CustomDropDown
            title="Order"
            items={["Ascending", "Descending"]}
            func={handleOrder}
          />

          <CustomDropDown
            title="Group"
            items={[
              "Group",
              "Hydroids",
              "Sponges",
              "Mammals",
              "Crustaceans",
              "Flatworms and Roundworms",
              "Fishes",
              "Insects",
              "Arachnids",
              "Annelid Worms",
              "Millipedes",
              "Reptiles",
              "Snails",
              "Clams",
              "Amphibians",
              "Birds",
            ]}
            func={handleGroupFilter}
            scroll
          />

          <CustomDropDown
            title="State"
            items={[
              "State",
              "AL",
              "AK",
              "AZ",
              "AR",
              "CA",
              "CO",
              "CT",
              "DE",
              "DC",
              "FL",
              "GA",
              "HI",
              "ID",
              "IL",
              "IN",
              "IA",
              "KS",
              "KY",
              "LA",
              "ME",
              "MD",
              "MA",
              "MI",
              "MN",
              "MS",
              "MO",
              "MT",
              "NE",
              "NV",
              "NH",
              "NJ",
              "NM",
              "NY",
              "NC",
              "ND",
              "OH",
              "OK",
              "OR",
              "PA",
              "RI",
              "SC",
              "SD",
              "TN",
              "TX",
              "UT",
              "VT",
              "VA",
              "WA",
              "WV",
              "WI",
              "WY",
              "AS",
              "GU",
              "MP",
              "PR",
              "VI",
            ]}
            func={handleStatesFilter}
            scroll
          />

          <CustomDropDown
            title="Endangered Status"
            items={[
              "Endangered Status",
              "Endangered",
              "Threatened",
              "Similarity of Appearance (Threatened)",
              "Not Listed",
              "Proposed Similarity of Appearance (Threatened)",
              "Recovery",
              "Original Data in Error - New Information Discovered",
              "Resolved Taxon",
              "Original Data in Error - Not a listable entity",
              "Original Data in Error - Taxonomic Revision",
              "Under Review",
              "Extinction",
              "Status Undefined",
              "Experimental Population, Non-Essential",
              "Proposed Endangered",
              "Pre-Act Delisted Taxon",
              "Species of Concern",
              "Proposed Threatened",
              "Candidate",
            ]}
            func={handleEndangeredFilter}
            scroll
          />
        </div>
        <button className="filter-submit-button" onClick={handleSubmit}>
          Submit
        </button>
        {loading && <Loading />}
        {!loading && (
          <React.Fragment>
            <div className="pagination-ind">
              <PaginationIndicator
                currPage={currPage}
                setCurrPage={setCurrPage}
                numPages={numPages}
              />
            </div>
            <div className="cardBox">
              {animals !== null &&
                animals.map((animal, index) => {
                  return (
                    <AnimalCard
                      key={index}
                      animal={animal}
                      highlight={highlight}
                    />
                  );
                })}
            </div>
            <PaginationIndicator
              currPage={currPage}
              setCurrPage={setCurrPage}
              numPages={numPages}
            />
          </React.Fragment>
        )}
      </div>
    </React.Fragment>
  );
}

export default Animals;
