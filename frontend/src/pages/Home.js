import React from "react";
import "../styles/Home.css";
import SplashImage from "../images/splash_image.jpg";

function Home() {
  return (
    <div className="home">
      <div
        className="homeContainer"
        style={{ backgroundImage: `url(${SplashImage})` }}
      >
        <h1 className="mainText">Explore the national parks around you</h1>
      </div>
    </div>
  );
}

export default Home;
