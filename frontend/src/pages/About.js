import "../styles/About.css";
import React, { useState, useEffect } from "react";
import TeamMemberCard from "../components/TeamMemberCard";
import {
  get_user_gitlab_information,
  get_overall_stats,
} from "../gitlab-api.js";
import { ToolsList } from "../data/ToolsInfo.js";
import ToolDetail from "../components/ToolDetail";

const About = () => {
  const [aboutData, setAboutData] = useState([]);
  const [overallStats, setOverallStats] = useState({});

  useEffect(() => {
    const fetchData = async () => {
      const data = await get_user_gitlab_information();
      setAboutData(data);
    };
    fetchData();
  }, []);

  useEffect(() => {
    const fetchStats = async () => {
      const data = await get_overall_stats();
      setOverallStats(data);
    };
    fetchStats();
  }, []);

  return (
    <div className="about">
      <h1 className="title">About Park Dex</h1>
      <div className="about-section">
        <h1>What is Park Dex?</h1>
        <p>
          Park Dex is an online platform that provides information about parks
          and animals across various states in the United States. Our goal is to
          make it easy for people to find information about their favorite parks
          and animals, as well as discover new places to explore. It is designed
          for people who love animals!
        </p>
      </div>
      <div className="about-section">
        <h1>How does Park Dex aggregate data?</h1>
        <p>
          At Park Dex, we use a variety of sources to gather information about
          parks and animals. This includes data from government agencies, as
          well as user-generated content from our community of users. We strive
          to provide the most accurate and up-to-date information possible, so
          that our users can plan their park visits with confidence.
        </p>
      </div>
      <div className="about-section">
        <h1>Meet the Team</h1>
        <div className="team-members">
          {aboutData.map((user) => (
            <TeamMemberCard
              key={user.gitlabUsername}
              fullName={user.fullName}
              img={user.img}
              gitlabUsername={user.gitlabUsername}
              email={user.email[0]}
              role={user.role}
              bio={user.bio}
              numCommits={user.numCommits}
              numIssues={user.numIssues}
              numUserTests={user.numUnitTests}
            />
          ))}
        </div>
      </div>
      <div className="about-section">
        <h1>Overall Stats</h1>
        <div className="overall-stats">
          <p>Total Commits: {overallStats.totalCommits}</p>
          <p>Total Issues: {overallStats.totalIssues}</p>
          <p>Total Issues: 0</p>
        </div>
        <h1 style={{ marginTop: "30px" }}>Tools Used</h1>
        <div className="tool-docs">
          {ToolsList.map((tool) => {
            return (
              <ToolDetail
                toolIcon={tool.icon}
                toolName={tool.name}
                toolLink={tool.link}
              />
            );
          })}
        </div>
        <div class="api-used-info">
          <h1 style={{ marginTop: "30px" }}>APIs scraped</h1>
          <p>
            <b>Wikipedia: </b> Compiling images of all the animals, gathering
            descriptions, information about parks.
          </p>
          <p>
            <b>NPS: </b> Finding what states and parks animals live in. Also
            finding a total list of parks.
          </p>
          <p>
            <b>Redlist: </b> Finding whether an animal is endangered or not
          </p>
          <p>
            <b>ECOS(US Fish and Wildlife Services): </b> Finding more
            information about each animal
          </p>
        </div>
        <div class="api-documentation" style={{ marginTop: "30px" }}>
          <a
            href="https://documenter.getpostman.com/view/25836986/2s93CEwwJX"
            target="_blank"
          >
            API Documentation
          </a>
        </div>
      </div>
    </div>
  );
};

export default About;
