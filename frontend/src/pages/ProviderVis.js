import React, { useState, useEffect } from "react";
import VineyardBarChart from "../components/visualizations/VineyardBarChart";
import WineTypePieChart from "../components/visualizations/WineTypePieChart";
import RegionTagsBarChart from "../components/visualizations/RegionTagsBarChart";
import "../styles/Visualizations.css";

const ProviderVis = () => {
  const [vineyardData, setVineyardData] = useState([]);
  const [wineData, setWineData] = useState([]);
  const [regionData, setRegionData] = useState([]);

  useEffect(() => {
    fetch("https://api.wineworld.me/vineyards")
      .then((response) =>
        response.json().then((jsonData) => {
          //onsole.log(jsonData);
          setVineyardData(processVineyardData(jsonData));
        })
      )
      .catch((error) => {
        console.error("Error fetching states data:", error);
      });

    fetch("https://api.wineworld.me/wines")
      .then((response) => response.json())
      .then((jsonData) => {
        //console.log(jsonData);
        setWineData(processWineData(jsonData));
      })
      .catch((error) => {
        console.error("Error fetching wines data:", error);
      });

    fetch("https://api.wineworld.me/regions")
      .then((response) => response.json())
      .then((jsonData) => {
        console.log(jsonData);
        setRegionData(processRegionData(jsonData));
      })
      .catch((error) => {
        console.error("Error fetching regions data:", error);
      });
  }, []);

  const processVineyardData = (data) => {
    const vineyardCount = {};

    data.list.forEach((vineyard) => {
      const country = vineyard.country;
      if (!vineyardCount[country]) {
        vineyardCount[country] = 1;
      } else {
        vineyardCount[country]++;
      }
    });
    return Object.keys(vineyardCount).map((country) => ({
      name: country,
      vineyards: vineyardCount[country],
    }));
  };

  const processWineData = (data) => {
    const wineTypeCount = {};

    data.list.forEach((wine) => {
      const type = wine.type;
      if (!wineTypeCount[type]) {
        wineTypeCount[type] = 1;
      } else {
        wineTypeCount[type]++;
      }
    });

    return Object.keys(wineTypeCount).map((type, index) => ({
      name: type,
      count: wineTypeCount[type],
    }));
  };

  const processRegionData = (data) => {
    const processedData = data.list.map((region) => ({
      name: region.name,
      tags: region.tags.length,
    }));

    //sort by alphabetical order
    return processedData.sort((a, b) => a.name.localeCompare(b.name));
  };

  return (
    <div className="container">
      <h1 className="title">Provider Visualizations</h1>
      <div className="charts-container">
        <h1 className="chart-title">Number of Vineyards per country</h1>
        <VineyardBarChart data={vineyardData} />
        <h2>Total number of each type of wine</h2>
        <WineTypePieChart data={wineData} />
        <h2>Total tags of each type of region</h2>
        <RegionTagsBarChart data={regionData} />
      </div>
    </div>
  );
};

export default ProviderVis;
