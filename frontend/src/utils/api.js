import axios from "axios";
import { frontend_website } from "../frontend_website";

export const getState = async (state_abbr) => {
  const response = await axios.get(
    frontend_website + `/api/states/${state_abbr}`
  );
  return response.data["data"];
};

export const getAnimal = async (animal_id) => {
  const response = await axios.get(
    frontend_website + `/api/animals/${animal_id}`
  );
  return response.data["data"];
};

export const getPark = async (park_id) => {
  const response = await axios.get(frontend_website + `/api/parks/${park_id}`);
  return response.data["data"];
};

export const searchModels = async (search_text) => {
  const search_text_encoded = encodeURIComponent(search_text);
  const response = await axios.get(
    frontend_website + `/api/search?search=${search_text_encoded}`
  );
  return response.data["data"];
};
