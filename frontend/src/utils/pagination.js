import axios from "axios";
import { frontend_website } from "../frontend_website";

export const get_states_length = async () => {
  const response = await axios.get(`${frontend_website}/api/states-length`);
  return response.data["count"];
};

export const fetch_states = async (page_num, sort, order, per_page) => {
  let website = `${frontend_website}/api/states?page=${page_num}`;
  if (sort) {
    website += `&sort_by=${encodeURIComponent(sort)}`;
  }
  if (order) {
    website += `&sort_order=${encodeURIComponent(order)}`;
  }
  if (per_page) {
    website += `&per_page=${encodeURIComponent(per_page)}`;
  }
  const response = await axios.get(website);
  return response.data["data"];
};

export const get_animals_length = async () => {
  const response = await axios.get(`${frontend_website}/api/animals-length`);
  return response.data["count"];
};

export const fetch_animals = async (
  page_num,
  sort,
  order,
  groupFilter,
  statesFilter,
  endangeredFilter,
  per_page
) => {
  let website = `${frontend_website}/api/animals?page=${page_num}`;
  if (sort) {
    website += `&sort_by=${encodeURIComponent(sort)}`;
  }
  if (order) {
    website += `&sort_order=${encodeURIComponent(order)}`;
  }
  if (groupFilter) {
    website += `&group=${encodeURIComponent(groupFilter)}`;
  }
  if (statesFilter) {
    website += `&states=${encodeURIComponent(statesFilter)}`;
  }
  if (endangeredFilter) {
    website += `&endangered_status=${encodeURIComponent(endangeredFilter)}`;
  }
  if (per_page) {
    website += `&per_page=${encodeURIComponent(per_page)}`;
  }
  const response = await axios.get(website);
  return response.data["data"];
};

export const get_parks_length = async () => {
  const response = await axios.get(`${frontend_website}/api/parks-length`);
  return response.data["count"];
};

export const fetch_parks = async (
  page_num,
  sort,
  order,
  activitiesFilter,
  statesFilter,
  per_page
) => {
  let website = `${frontend_website}/api/parks?page=${page_num}`;
  if (sort) {
    website += `&sort_by=${encodeURIComponent(sort)}`;
  }
  if (order) {
    website += `&sort_order=${encodeURIComponent(order)}`;
  }
  if (activitiesFilter) {
    website += `&activities=${encodeURIComponent(activitiesFilter)}`;
  }
  if (statesFilter) {
    website += `&states=${encodeURIComponent(statesFilter)}`;
  }
  if (per_page) {
    website += `&per_page=${encodeURIComponent(per_page)}`;
  }
  console.log(website);
  const response = await axios.get(website);
  return response.data["data"];
};

export const search_model = async (search_text, model) => {
  console.log("Seach Text", search_text);
  const search_text_encoded = encodeURIComponent(search_text);
  const response = await axios.get(
    `${frontend_website}/api/search?search=${search_text_encoded}&model=${model}`
  );
  return response.data["data"];
};
