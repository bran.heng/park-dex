import "./App.css";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import NavBar from "./components/Navbar";
import About from "./pages/About";
import Home from "./pages/Home";
import Parks from "./pages/Parks/Parks.js";
import States from "./pages/States/States.js";
import Animals from "./pages/Animals/Animals.js";
import StateInstance from "./pages/States/StateInstance.js";
import ParkInstance from "./pages/Parks/ParkInstance.js";
import AnimalInstance from "./pages/Animals/AnimalInstance.js";
import SearchPage from "./pages/SearchPage.js";
//import visualizations
import OurVis from "./pages/OurVis";
import ProviderVis from "./pages/ProviderVis";

import Footer from "./components/Footer";

import "bootstrap/dist/css/bootstrap.min.css";

function App() {
  return (
    <div className="App">
      <Router>
        <NavBar />
        <div>
          <Routes>
            <Route path="/" exact element={<Home />} />
            <Route path="/about" element={<About />} />
            <Route path="/parks" element={<Parks />} />
            <Route path="/parks/:park_id" element={<ParkInstance />} />
            <Route path="/states" element={<States />} />
            <Route path="/states/:state_abbr" element={<StateInstance />} />
            <Route path="/animals" element={<Animals />} />
            <Route path="/animals/:animal_id" element={<AnimalInstance />} />
            <Route path="/search/:search_term" element={<SearchPage />} />
            <Route path="/visualizations" element={<OurVis />} />
            <Route path="/providervisuals" element={<ProviderVis />} />
          </Routes>
        </div>
      </Router>
      <Footer />
    </div>
  );
}

export default App;
