import axios from "axios";
import Alex from "./images/profile-pics/alex.png";
import Dev from "./images/profile-pics/dev.jpg";

const users = [
  {
    fullName: "Chaitanya Eranki",
    headshot:
      "https://miro.medium.com/v2/resize:fit:720/format:webp/1*Y48-1i1oN6US7iJrI8_uZQ.jpeg",
    gitlabUsername: "Chai77",
    email: ["chaitanyaeranki@y7mail.com", "eranki.srinivasa@gmail.com"],
    role: "Frontend",
    unitTests: 31,
    bio: "I am a third-year CS major at the University of Texas at Austin. I enjoy playing chess and watching movies.",
  },
  {
    fullName: "Branden Heng",
    headshot:
      "https://miro.medium.com/v2/resize:fit:640/format:webp/1*cNV9J7ygNFZ-h2VIVEGJbQ.png",
    gitlabUsername: "bran.heng",
    email: [
      "branden.heng@utexas.edu",
      "brandenheng@wireless-10-155-30-54.public.utexas.edu",
      "brandenheng@Brandens-MBP.attlocal.net",
      "brandenheng@wireless-10-155-251-77.public.utexas.edu",
    ],
    role: "Backend and API",
    unitTests: 9,
    bio: "I’m a junior CS Major at UT Austin. I’m super passionate about game development and the arts.",
  },
  {
    fullName: "Alex Castillo",
    headshot: Alex,
    gitlabUsername: "alexcastillo333",
    email: [
      "a817castillo@gmail.com",
      "98500283+alexcastillo96@users.noreply.github.com",
    ],
    role: "Backend",
    unitTests: 0,
    bio: "I am a junior CS major at UT Austin. I enjoy soccer and biking.",
  },
  {
    fullName: "Akash Aedavelli",
    headshot:
      "https://miro.medium.com/v2/resize:fit:532/format:webp/1*eYbU7G8nmSIWz7kwqMYTHw.jpeg",
    gitlabUsername: "akashra",
    email: ["akash.aedavelli@gmail.com", "akash.aedavelli@utexas.edu"],
    role: "Frontend",
    unitTests: 0,
    bio: "I'm a third year computer science major at the University of Texas at Austin. I enjoy rock climbing, skating, and watching anime.",
  },
  {
    fullName: "Dev Randalpura",
    headshot: Dev,
    gitlabUsername: "devr0306",
    email: ["devr0306@gmail.com"],
    role: "Frontend",
    unitTests: 0,
    bio: "I’m a sophomore CS major at UT. I enjoy playing basketball, working out, and hanging out with friends.",
  },
];

export const get_user_gitlab_information = async () => {
  const user_information = [];

  const commit_response = await get_commits();
  const commit_data = {};
  for (let i = 0; i < commit_response.length; i++) {
    let commit_has_person = false;
    for (let k = 0; k < users.length; k++) {
      if (
        users[k].email.includes(commit_response[i]["email"]) ||
        users[k].fullName === commit_response[i]["name"]
      ) {
        commit_has_person = true;
        if (users[k].gitlabUsername in commit_data) {
          commit_data[users[k].gitlabUsername] += commit_response[i]["commits"];
        } else {
          commit_data[users[k].gitlabUsername] = commit_response[i]["commits"];
        }
      }
    }
    if (!commit_has_person) {
      console.error(
        "This commit has no person associated with it ",
        commit_response[i]
      );
    }
  }

  const issues_response = await get_issues();
  const issue_data = {};
  for (let i = 0; i < issues_response.length; i++) {
    const username = issues_response[i]["author"]["username"];
    if (username in issue_data) {
      issue_data[username] += 1;
    } else {
      issue_data[username] = 1;
    }
  }

  for (let i = 0; i < users.length; i++) {
    user_information.push({
      fullName: users[i].fullName,
      img: users[i].headshot,
      gitlabUsername: users[i].gitlabUsername,
      email: users[i].email,
      role: users[i].role,
      bio: users[i].bio,
      numCommits:
        users[i].gitlabUsername in commit_data
          ? commit_data[users[i].gitlabUsername]
          : 0,
      numIssues:
        users[i].gitlabUsername in issue_data
          ? issue_data[users[i].gitlabUsername]
          : 0,
      numUnitTests: users[i].unitTests,
    });
  }

  return user_information;
};

export const get_overall_stats = async () => {
  const commit_data = await get_commits();
  const issue_data = await get_issues();
  let totalCommits = 0;
  for (let i = 0; i < commit_data.length; i++) {
    totalCommits += commit_data[i]["commits"];
  }
  return {
    totalCommits: totalCommits,
    totalIssues: issue_data.length,
    totalUnitTests: users.reduce(
      (partialSum, user) => partialSum + user.unitTests,
      0
    ),
  };
};

const get_commits = async () => {
  // let response = axios.get("https://gitlab.com/api/v4/projects/43393995/repository/commits");
  let response = axios.get(
    "https://gitlab.com/api/v4/projects/43393995/repository/contributors"
  );
  let data = await response;
  return data.data;
};

const get_issues = async () => {
  let response = axios.get(
    "https://gitlab.com/api/v4/projects/43393995/issues?scope=all&per_page=1000"
  );
  let data = await response;
  return data.data;
};
