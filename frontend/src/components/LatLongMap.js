import React, { useEffect, useRef } from "react";
import mapboxgl from "mapbox-gl";

// eslint-disable-next-line import/no-webpack-loader-syntax, import/no-unresolved
mapboxgl.workerClass = require('worker-loader!mapbox-gl/dist/mapbox-gl-csp-worker').default;
mapboxgl.accessToken =
  "pk.eyJ1IjoiY2hhaXRhbnlhZXJhbmtpIiwiYSI6ImNsZXh2OXVycjA4N2Mzc28yaXEzMmt3NGsifQ.ePuV8zVdC0J1p2kNuQv0xA";

const LatLongMap = ({ latitude, longitude }) => {
  const mapContainer = useRef(null);
  const map = useRef(null);

  useEffect(() => {
    // Create the map object
    map.current = new mapboxgl.Map({
      container: mapContainer.current,
      style: "mapbox://styles/mapbox/streets-v11",
      center: [longitude, latitude],
      zoom: 8,
    });

    // Cleanup function to remove the map from the DOM when unmounting
    return () => {
      if (map.current) {
        map.current.remove();
      }
    };
  }, [latitude, longitude]);

  useEffect(() => {
    // Update the map center when the latitude or longitude props change
    if (map.current) {
      map.current.setCenter([longitude, latitude]);
    }
  }, [latitude, longitude]);

  return <div ref={mapContainer} style={{ height: "500px", width: "100%" }} />;
};

export default LatLongMap;
