import React from "react";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import { Link } from "react-router-dom";
import { highlightWords } from "../../utils/highlighting";

const DESCRIPTION_CHARS = 100;
const NUM_ANIMALS_TO_SHOW = 3;

function ParkCard({ park, highlight }) {
  const {
    id,
    full_name,
    parkCode,
    url,
    description,
    image1Url,
    animals_relate,
    states_relate,
  } = park;
  const highlightTerms = highlight.split(" ");

  return (
    <Card style={{ width: "18rem" }}>
      <Card.Img
        variant="top"
        src={
          image1Url == null || image1Url.endsWith("Red_Pencil_Icon.png")
            ? "./placeholders/animal_placeholder.webp"
            : image1Url
        }
        style={{ height: 300 }}
      />
      <Card.Body>
        {/* Highlighting */}
        <Card.Title
          dangerouslySetInnerHTML={{
            __html: highlightWords(full_name, highlightTerms),
          }}
        />
        <Card.Subtitle
          dangerouslySetInnerHTML={{
            __html: highlightWords(parkCode, highlightTerms),
          }}
        />
        <Card.Text>
          <a href={url}>{url}</a>
        </Card.Text>
        <Card.Text>
          States:{" "}
          {states_relate.map((state, index) => {
            return (
              <React.Fragment key={index}>
                {state.name}
                {index === states_relate.length - 1 ? "" : ", "}
              </React.Fragment>
            );
          })}
        </Card.Text>

        {/* Description */}
        <Card.Text>
          {description && (
            <div className="park-description">
              {description.length > DESCRIPTION_CHARS
                ? description.substring(0, DESCRIPTION_CHARS) + "..."
                : description}
            </div>
          )}
          {!description && "There is no description for this park!"}
        </Card.Text>

        <Card.Text>Number of animals: {animals_relate.length}</Card.Text>

        {/* List of animals that appear in Park */}
        <Card.Text>
          Animals:{" "}
          {animals_relate.map((animal, index) => {
            if (index === NUM_ANIMALS_TO_SHOW - 1) {
              return <React.Fragment key={index}>{animal.name}</React.Fragment>;
            } else if (index < NUM_ANIMALS_TO_SHOW - 1) {
              if (index < animals_relate.length - 1) {
                return (
                  <React.Fragment key={index}>
                    {animal.name + ", "}
                  </React.Fragment>
                );
              } else {
                return (
                  <React.Fragment key={index}>
                    {animal.name + ", "}
                  </React.Fragment>
                );
              }
            } else {
              return <React.Fragment key={index}></React.Fragment>;
            }
          })}
          {animals_relate.length > NUM_ANIMALS_TO_SHOW ? "..." : ""}
        </Card.Text>

        <Link to={"/parks/" + id}>
          <Button variant="primary">More Info</Button>
        </Link>
      </Card.Body>
    </Card>
  );
}

export default ParkCard;
