import React from "react";
import "../../styles/ParkDetails.css";
import LatLongMap from "../LatLongMap";
import SliderCard from "../SliderCard";

function ParkDetails({ park, showLatLong }) {
  const {
    full_name,
    parkCode,
    url,
    states_relate,
    animals_relate,
    latLong,
    description,
    activities,
    phoneNumbers,
    image1Url,
    weatherInfo,
    directionInfo,
    entranceFees,
  } = park;

  // Get lattitude and longitude coordinates
  const generateLatLong = (latLong) => {
    const result = [];
    const latLongSplit = latLong.split(" ");
    result.push(parseFloat(latLongSplit[0].split(":")[1]));
    result.push(parseFloat(latLongSplit[1].split(":")[1]));
    return result;
  };
  const [lat, long] = generateLatLong(latLong);

  return (
    <div className="details-container">
      <h1 className="state-name">{full_name}</h1>
      <img className="state-image" src={image1Url} alt={`${full_name}`} />
      <div className="state-info">
        <p>
          <span>Park Code: </span>
          {parkCode}{" "}
        </p>
        <p>
          <span>url: </span>
          <a href={url}>{url}</a>{" "}
        </p>
        <p>
          <span>Entrance fees: </span>
          {entranceFees.map((entranceFee, index) => {
            return (
              <React.Fragment key={index}>
                ${entranceFee}
                {index === entranceFees.length - 1 ? "" : ", "}
              </React.Fragment>
            );
          })}
        </p>
        <p>States:</p>
        <div className="slider">
          {states_relate.map((state, index) => (
            <div className="slider-card" key={index}>
              <SliderCard
                props={{
                  name: state.name,
                  link: "/states/" + state.id.toLowerCase(),
                  image: state.image,
                }}
              />
            </div>
          ))}
        </div>
        <p>
          <span>Description: </span> {description}
        </p>
        <p>
          <span>Animals: </span>

          <div className="slider">
            {animals_relate.map((animal, index) => (
              <div className="slider-card" key={index}>
                <SliderCard
                  props={{
                    name: animal.name,
                    link: "/animals/" + animal.id.toLowerCase(),
                    image: animal.image,
                  }}
                />
              </div>
            ))}
          </div>
        </p>
        <div>
          <span>Activities: </span>
          <ul>
            {activities.map((activity, index) => {
              return <li key={index}>{activity}</li>;
            })}
          </ul>
        </div>
        <p>
          <span>Climate: </span> {weatherInfo}
        </p>
        <p>
          <span>Phone Numbers: </span>
          {phoneNumbers.map((phoneNumber, index) => {
            return (
              <React.Fragment key={index}>
                {phoneNumber}
                {index === phoneNumbers.length - 1 ? "" : ", "}
              </React.Fragment>
            );
          })}
        </p>
        <p>
          <span>Direction Info: </span> {directionInfo}
        </p>
      </div>
      {showLatLong && <LatLongMap latitude={lat} longitude={long} />}
    </div>
  );
}

export default ParkDetails;
