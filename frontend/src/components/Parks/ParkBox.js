import React from "react";
import ParkCard from "./ParkCard.js";

const ParkBox = ({ parks, highlight }) => {
  return (
    <div className="cardBox">
      {parks !== null &&
        parks.map((park, index) => {
          return <ParkCard key={index} park={park} highlight={highlight} />;
        })}
    </div>
  );
};

export default ParkBox;
