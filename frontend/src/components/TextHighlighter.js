import React from "react";
import Highlighter from "react-highlight-words";

const TextHighlighter = ({ text, searchWords }) => {
  return (
    <div>
      <Highlighter
        highlightClassName="highlight"
        searchWords={searchWords}
        autoEscape={true}
        textToHighlight={text}
      />
    </div>
  );
};

export default TextHighlighter;
