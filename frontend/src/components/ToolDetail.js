import PropTypes from "prop-types";
import React from "react";

import Card from "react-bootstrap/Card";
import "../styles/ToolDetail.css";

function ToolDetail(props) {
  const { toolName, toolIcon, toolLink } = props;

  return (
    <Card style={{ width: "150px" }}>
      <Card.Img
        variant="top"
        src={toolIcon}
        height="150px"
        style={{ padding: "25px" }}
      />
      <a href={toolLink}>
        <Card.Title style={{ textAlign: "center" }}>{toolName}</Card.Title>
      </a>
    </Card>
  );
}

ToolDetail.propTypes = {
  toolName: PropTypes.string.isRequired,
  toolIcon: PropTypes.string.isRequired,
  toolLink: PropTypes.string.isRequired,
};

export default ToolDetail;
