import { React, useState } from "react";
import "../styles/Parks.css";

const SearchBar = ({ model, custom_styles, handleSearch }) => {
  const handleChange = (event) => {
    setMessage(event.target.value);
  };

  const [message, setMessage] = useState("");

  return (
    <div className="active-pink-3 active-pink-4">
      <input
        className="parks-search-bar"
        type="text"
        placeholder={"Search " + model}
        aria-label={"Search " + model}
        onChange={handleChange}
        value={message}
      />
      <button
        className="parks-search-button"
        onClick={() => {
          if (handleSearch) {
            handleSearch(message);
          }
          setMessage("");
        }}
      >
        Search
      </button>
    </div>
  );
};

export default SearchBar;
