import React from "react";
import { MDBFooter } from "mdb-react-ui-kit";

const Footer = () => {
  return (
    <MDBFooter bgColor="dark" className="text-center text-lg-left">
      <div
        className="text-center p-3"
        style={{ backgroundColor: "rgba(0, 0, 0, 0.2)", color: "white" }}
      >
        &copy; {new Date().getFullYear()} Copyright:{" "}
        <a className="text-light" href="https://park-dex.me">
          Park-Dex.com
        </a>
      </div>
    </MDBFooter>
  );
};

export default Footer;
