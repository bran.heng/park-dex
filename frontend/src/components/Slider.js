import React from "react";
import SliderCard from "./SliderCard";

const Slider = (props) => {
  const containerRef = React.useRef(null);
  const styles = {
    hidescroll: {
      width: "100%",
      overflow: "hidden",
    },
    container: {
      width: "100%",
      overflowX: "auto",
      paddingBottom: 20,
      marginBottom: -20,
    },
  };

  const goTo = (n) => {
    containerRef.current.scrollLeft =
      containerRef.current.children[0].children[n - 1].offsetLeft;
  };

  const goToByElem = (elem) => {
    containerRef.current.scrollLeft = elem.offsetLeft;
  };

  const handleClick = (e) => {
    goToByElem(e.target);
  };

  return (
    <div className="slider-wrapper" style={styles.hidescroll}>
      <div
        className="slider-container"
        style={styles.container}
        ref={containerRef}
      >
        {/* <ul className="slider-list"></ul> */}
        {props.items.map((item, index) => {
          <SliderCard
            props={{
              link: props.type + item.id,
              name: item.name,
              image: item.image,
            }}
          />;
        })}
      </div>
    </div>
  );
};

export default Slider;
