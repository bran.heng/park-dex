import React, { useState } from "react";
import USAMap from "react-usa-map";

const AnimalMap = ({ states }) => {
  const style = { fill: "#3b3b87", stroke: "#666", strokeWidth: 2 };

  const stateMap = {};
  states.forEach((state) => {
    stateMap[state] = style;
  });

  return (
    <div>
      <USAMap customize={stateMap} />
    </div>
  );
};

export default AnimalMap;
