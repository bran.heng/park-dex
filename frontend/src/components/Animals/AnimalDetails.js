import React from "react";
import "../../styles/DetailsCard.css";
import AnimalMap from "./AnimalMap";
import AnimalPlaceholder from "../../images/placeholders/animal_placeholder.webp";
import SliderCard from "../SliderCard";

function AnimalDetails({ animal }) {
  return (
    <div className="details-container">
      <h1 className="state-name">{animal.common_name}</h1>
      <img
        className="state-image"
        src={
          animal.image1url == null ||
          animal.image1url.endsWith("Red_Pencil_Icon.png")
            ? AnimalPlaceholder
            : animal.image1url
        }
        alt={`${animal.common_name}`}
      />
      <div className="state-info">
        <p>
          <span>Class: </span>
          {animal.species_group}
        </p>
        <p>
          <span>Genus species: </span>
          <span style={{ fontStyle: "italic", fontWeight: "normal" }}>
            {animal.scientific_name}
          </span>
        </p>
        <p>
          <span>Description: </span>
          {animal.description == null
            ? "There is no description"
            : animal.description}
        </p>
        <p>
          <span>National parks: </span>
          <div className="slider">
            {animal.parks_relate.map((park, index) => (
              <div className="slider-card" key={index}>
                <SliderCard
                  props={{
                    name: park.name,
                    link: "/parks/" + park.id,
                    image: park.image,
                  }}
                />
              </div>
            ))}
          </div>
        </p>
        <p>States:</p>
        <div className="slider">
          {animal.states_relate.map((state, index) => (
            <div className="slider-card" key={index}>
              <SliderCard
                props={{
                  name: state.name,
                  link: "/states/" + state.id.toLowerCase(),
                  image: state.image,
                }}
              />
            </div>
          ))}
        </div>
        <p>
          <span>Endangered Status: </span>
          {animal.endangered_status}
        </p>
      </div>
      <AnimalMap
        states={animal.states_relate.map((state) => {
          return state.id;
        })}
      />
    </div>
  );
}

export default AnimalDetails;
