import React from "react";
import AnimalCard from "./AnimalCard";

const AnimalBox = ({ animals, highlight }) => {
  console.log("Aninmals", animals);
  return (
    <div className="cardBox">
      {animals !== null &&
        animals.map((animal, index) => {
          return (
            <AnimalCard key={index} animal={animal} highlight={highlight} />
          );
        })}
    </div>
  );
};

export default AnimalBox;
