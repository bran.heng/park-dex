import React from "react";
import { Link } from "react-router-dom";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import AnimalPlaceholder from "../../images/placeholders/animal_placeholder.webp";
import { highlightWords } from "../../utils/highlighting.js";

const DESCRIPTION_CHARS = 100;
const NUM_PARKS_TO_SHOW = 2;
const NUM_STATES_TO_SHOW = 2;

function AnimalCard({ animal, highlight }) {
  // parks
  const {
    id,
    common_name,
    scientific_name,
    species_group,
    states_relate,
    parks_relate,
    endangered_status,
    description,
    image1url,
  } = animal;

  const highlightTerms = highlight.split(" ");

  return (
    <Card style={{ width: "18rem" }}>
      <Card.Img
        variant="top"
        src={
          image1url == null || image1url.endsWith("Red_Pencil_Icon.png")
            ? AnimalPlaceholder
            : image1url
        }
        style={{ height: 300 }}
      />
      <Card.Body>
        <Card.Title
          dangerouslySetInnerHTML={{
            __html: highlightWords(common_name, highlightTerms),
          }}
        />
        <Card.Subtitle
          className="animal-genus-species"
          dangerouslySetInnerHTML={{
            __html: highlightWords(scientific_name, highlightTerms),
          }}
        />
        <Card.Text>
          {description && (
            <div className="animal-description">
              {description.length > DESCRIPTION_CHARS
                ? description.substring(0, DESCRIPTION_CHARS) + "..."
                : description}
            </div>
          )}
          {!description && "There is no description for this animal!"}
          <div className="animal-species-class">
            <b>Class:</b> {species_group}
          </div>
          <div className="animal-endangered-status">
            <b>Endangered Status:</b> {endangered_status}
          </div>
          <div className="animal-parks">
            <b>Parks:</b>{" "}
            {parks_relate != null &&
              parks_relate.length > 0 &&
              parks_relate.map((park, index) => {
                if (index === NUM_PARKS_TO_SHOW - 1) {
                  return (
                    <React.Fragment key={index}>{park.name}</React.Fragment>
                  );
                } else if (index < NUM_PARKS_TO_SHOW - 1) {
                  if (index < parks_relate.length - 1) {
                    return (
                      <React.Fragment key={index}>
                        {park.name + ", "}
                      </React.Fragment>
                    );
                  } else {
                    return (
                      <React.Fragment key={index}>
                        {park.name}
                      </React.Fragment>
                    );
                  }
                } else {
                  return <React.Fragment key={index}></React.Fragment>;
                }
              })}
            {parks_relate != null &&
            parks_relate.length > 0 &&
            parks_relate.length > NUM_PARKS_TO_SHOW
              ? "..."
              : ""}
            {parks_relate != null &&
              parks_relate.length === 0 &&
              "There are no parks"}
          </div>
          <div className="animal-states">
            <b>States:</b>{" "}
            {states_relate != null &&
              states_relate.map((state, index) => {
                if (index === NUM_STATES_TO_SHOW - 1) {
                  return (
                    <React.Fragment key={index}>{state.name}</React.Fragment>
                  );
                } else if (index < NUM_STATES_TO_SHOW - 1) {
                    if (index < states_relate.length - 1) {
                      return (
                        <React.Fragment key={index}>
                          {state.name + ", "}
                        </React.Fragment>
                      );
                    } else {
                        return (
                          <React.Fragment key={index}>
                            {state.name}
                          </React.Fragment>
                        );
                    }
                } else {
                  return <React.Fragment></React.Fragment>;
                }
              })}
            {states_relate != null && states_relate.length > NUM_STATES_TO_SHOW
              ? "..."
              : ""}
          </div>
        </Card.Text>
        <Link to={`/animals/${id}`}>
          <Button variant="primary">View Animal</Button>
        </Link>
      </Card.Body>
    </Card>
  );
}

export default AnimalCard;
