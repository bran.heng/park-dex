import PropTypes from "prop-types";
import React from "react";

function TeamMemberCard(props) {
  const {
    fullName,
    img,
    gitlabUsername,
    email,
    role,
    numCommits,
    numIssues,
    numUserTests,
    bio,
  } = props;

  return (
    <div className="team-member-card">
      <div className="team-member-card__header">
        <h2 className="team-member-card__name">{fullName}</h2>
      </div>

      <img className="team-member-image" src={img} alt={fullName + " image"} />

      <div className="team-member-card__body">
        <p className="team-member-card__gitlab">
          {gitlabUsername} ({email}) [{role}]
        </p>
      </div>

      <div className="team-member-card__body">
        <p className="team-member-card__gitlab">{bio}</p>
      </div>

      <div className="team-member-card__stats">
        <div className="team-member-card__stat">
          <span className="team-member-card__stat-value">{numCommits}</span>
          <span className="team-member-card__stat-label"> Commits</span>
        </div>
        <div className="team-member-card__stat">
          <span className="team-member-card__stat-value">{numIssues}</span>
          <span className="team-member-card__stat-label"> Issues</span>
        </div>
        <div className="team-member-card__stat">
          <span className="team-member-card__stat-value">{numUserTests}</span>
          <span className="team-member-card__stat-label"> User Tests</span>
        </div>
      </div>
    </div>
  );
}

TeamMemberCard.propTypes = {
  fullName: PropTypes.string.isRequired,
  gitlabUsername: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  numCommits: PropTypes.number.isRequired,
  numIssues: PropTypes.number.isRequired,
};

export default TeamMemberCard;
