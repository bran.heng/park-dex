import React, { useState } from "react";
import "../styles/Navbar.css";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import { Link } from "react-router-dom";
import NavIcon from "../art/park-dex-full-icon.png";

function NavBar() {
  const handleSearch = (event) => {
    setMessage("");
  };

  const handleChange = (event) => {
    setMessage(event.target.value);
  };

  const [message, setMessage] = useState("");

  return (
    <Navbar
      className="navbar-container"
      bg="dark"
      expand="lg"
      data-testid="navbar"
    >
      <Container>
        <Navbar.Brand href="/">
          <img
            src={NavIcon}
            alt="ParkDex"
            style={{ height: "34px", width: "82px" }}
          />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link href="/">Home</Nav.Link>
            <Nav.Link href="/about">About</Nav.Link>
            <Nav.Link href="/parks">Parks</Nav.Link>
            <Nav.Link href="/animals">Animals</Nav.Link>
            <Nav.Link href="/states">States</Nav.Link>
            <Nav.Link href="/visualizations">Visualizations</Nav.Link>
            <Nav.Link href="/providervisuals">Provider Visuals</Nav.Link>
          </Nav>
        </Navbar.Collapse>
        <div className="nav-search nav-search-typing">
          <input
            className="nav-search-bar"
            type="text"
            placeholder={"Search"}
            aria-label={"Search"}
            onChange={handleChange}
            value={message}
          />
          <Link to={"/search/" + message}>
            <button className="nav-search-button" onClick={handleSearch}>
              Search
            </button>
          </Link>
        </div>
      </Container>
    </Navbar>
  );
}

export default NavBar;
