import React from "react";
import Card from "react-bootstrap/Card";
import "../styles/SliderCard.css";

function SliderCard({ props }) {
  return (
    <Card
      style={{
        backgroundColor: "#8499B1",
        alignItems: "center",
        borderRadius: 15,
      }}
    >
      <Card.Img
        variant="top"
        src={props.image}
        style={{ height: "280px", width: "250px" }}
      />
      <div className="slider-card-body">
        <a href={props.link} className="slider-card-name">
          {props.name}
        </a>
      </div>
    </Card>
  );
}

export default SliderCard;
