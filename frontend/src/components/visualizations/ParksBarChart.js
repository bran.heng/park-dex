// ParkActivitiesBarChart.js
import React from "react";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
} from "recharts";

const ParksBarChart = ({ data }) => {
  return (
    <BarChart
      width={1000}
      height={500}
      data={data}
      margin={{
        top: 5,
        right: 30,
        left: 20,
        bottom: 5,
      }}
    >
      <CartesianGrid strokeDasharray="3 3" />
      <XAxis dataKey="park" />
      <YAxis />
      <Tooltip />
      <Legend />
      <Bar dataKey="activities" fill="#8884d8" />
    </BarChart>
  );
};

export default ParksBarChart;
