import React from "react";
import StateCard from "./StateCard.js";

const StateBox = ({ states, highlight }) => {
  return (
    <div className="cardBox">
      {states !== null &&
        states.map((state, index) => {
          return <StateCard key={index} data={state} highlight={highlight} />;
        })}
    </div>
  );
};

export default StateBox;
