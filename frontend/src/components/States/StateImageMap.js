import React from "react";
import USAMap from "react-usa-map";

const StateImageMap = ({ stateCode }) => {
  // Get the state code for the state
  stateCode = stateCode.toUpperCase();

  return (
    <USAMap
      customize={{
        [stateCode]: { fill: "#703638", stroke: "#666", strokeWidth: 2 },
      }}
      defaultFillColor="#f0f0f0"
      defaultStrokeColor="#666"
      defaultStrokeWidth={2}
    />
  );
};

export default StateImageMap;
