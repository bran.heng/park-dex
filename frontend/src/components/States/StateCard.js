import React from "react";
import { Link } from "react-router-dom";
import "../../styles/StateCard.css";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import StatePlaceholder from "../../images/placeholders/state_placeholder.jpg";
import { highlightWords } from "../../utils/highlighting.js";

const NUM_ANIMALS_TO_SHOW = 3;
const NUM_PARKS_TO_SHOW = 2;

function StateCard({ data, highlight }) {
  const highlightTerms = highlight.split(" ");
  return (
    <Card style={{ width: "18rem" }}>
      <Card.Img
        variant="top"
        src={data.image == null ? StatePlaceholder : data.image}
        style={{ height: 300 }}
      />
      <Card.Body>
        <Card.Title
          dangerouslySetInnerHTML={{
            __html: highlightWords(data.name, highlightTerms),
          }}
        />
        <Card.Subtitle
          dangerouslySetInnerHTML={{
            __html: highlightWords(data.abbreviation, highlightTerms),
          }}
        />

        <Card.Text>Number of animals: {data.animals.length}</Card.Text>

        {/* List of animals that appear in State */}
        <Card.Text>
          Animals:{" "}
          {data.animals.map((animal, index) => {
            if (index === NUM_ANIMALS_TO_SHOW - 1) {
              return (
                <React.Fragment key={index}>
                  {animal.common_name}
                </React.Fragment>
              );
            } else if (index < NUM_ANIMALS_TO_SHOW - 1) {
              if (index < data.animals.length - 1) {
                return (
                  <React.Fragment key={index}>
                    {animal.common_name + ", "}
                  </React.Fragment>
                );
              } else {
                return (
                  <React.Fragment key={index}>
                    {animal.common_name}
                  </React.Fragment>
                );
              }
            } else {
              return <React.Fragment key={index}></React.Fragment>;
            }
          })}
          {data.animals.length > NUM_ANIMALS_TO_SHOW ? "..." : ""}
        </Card.Text>

        <Card.Text>Number of parks: {data.parks.length}</Card.Text>

        {/* List of parks that appear in State */}
        <Card.Text>
          National Parks:{" "}
          {data.parks.length > 0 && (
            <React.Fragment>
              {data.parks.map((park, index) => {
                if (index === NUM_PARKS_TO_SHOW - 1) {
                  return (
                    <React.Fragment key={index}>
                      {park.park_name}
                    </React.Fragment>
                  );
                } else if (index < NUM_PARKS_TO_SHOW - 1) {
                  if (index < data.parks.length - 1) {
                    return (
                      <React.Fragment key={index}>
                        {park.park_name + ", "}
                      </React.Fragment>
                    );
                  } else {
                    return (
                      <React.Fragment key={index}>
                        {park.park_name}
                      </React.Fragment>
                    );
                  }
                } else {
                  return <React.Fragment key={index}></React.Fragment>;
                }
              })}
              {data.parks.length > NUM_PARKS_TO_SHOW ? "..." : ""}
            </React.Fragment>
          )}
          {data.parks.length === 0 && "No national parks in state"}
        </Card.Text>

        <Card.Text>Area: {data.landArea}</Card.Text>

        <Link to={"/states/" + data.id}>
          <Button variant="primary">More Info</Button>
        </Link>
      </Card.Body>
    </Card>
  );
}

export default StateCard;
