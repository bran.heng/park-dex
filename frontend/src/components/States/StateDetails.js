import React from "react";
import { Link } from "react-router-dom";
import "../../styles/DetailsCard.css";
import StateImageMap from "./StateImageMap";
import StatePlaceholder from "../../images/placeholders/state_placeholder.jpg";
import SliderCard from "../SliderCard";

function StateDetails({ state }) {
  return (
    <div className="details-container">
      <h1 className="state-name">{state.name}</h1>
      <img
        className="state-image"
        src={state.image == null ? StatePlaceholder : state.image}
        alt={`${state.name}`}
      />
      <div className="state-info">
        <p>
          <span>Number of National Parks: </span>
          {state.parks.length}
        </p>
        <p>National Parks: </p>
        <div className="slider">
          {state.parks.map((park, index) => (
            <div className="slider-card" key={index}>
              <SliderCard
                props={{
                  name: park.name,
                  link: "/parks/" + park.id,
                  image: park.image,
                }}
              />
            </div>
          ))}
        </div>

        <p>Animals:</p>
        <div className="slider">
          {state.animals.map((animal, index) => (
            <div className="slider-card" key={index}>
              <SliderCard
                props={{
                  name: animal.name,
                  link: "/animals/" + animal.id.toLowerCase(),
                  image: animal.image,
                }}
              />
            </div>
          ))}
        </div>

        <p>
          <span>Land Area: </span>
          {state.landArea} sq mi
        </p>
        <p>
          <span>Water Area: </span>
          {state.waterArea} sq mi
        </p>
        <p>
          <span>Description: </span>
          {state.description}
        </p>
      </div>
      <StateImageMap stateCode={state.id} />
    </div>
  );
}

export default StateDetails;
