export const AnimalsList = [
  {
    commonName: "Coyote",
    urlname: "Coyote",
    class: "Mammalia",
    genusSpecies: "Canis latrans",
    parks: ["Big Bend", "Grand Canyon", "White Sands"],
    states: ["Texas", "Arizona", "New Mexico"],
    endangeredStatus: "least concern",
    regions: [
      "Upper Colorado Basin",
      "Lower Colorado Basin",
      "Arkansas-Rio Grande-Texas-Gulf",
    ],
    description:
      "The coyote (Canis latrans) is a species of canine native to North America. It is smaller than its close relative, the wolf, and slightly smaller than the closely related eastern wolf and red wolf. It fills much of the same ecological niche as the golden jackal does in Eurasia. The coyote is larger and more predatory and was once referred to as the American jackal by a behavioral ecologist. Other historical names for the species include the prairie wolf and the brush wolf.",
    image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9c/2009-Coyote-Yosemite.jpg/440px-2009-Coyote-Yosemite.jpg",
    image2:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/73/Coyote_portrait.jpg/440px-Coyote_portrait.jpg",
  },
  {
    commonName: "American Black Bear",
    urlname: "AmericanBlackBear",
    class: "Mammalia",
    genusSpecies: "Ursus americanus",
    parks: ["Big Bend", "Grand Canyon", "Grand Teton"],
    states: ["Texas", "Arizona", "Wyoming"],
    endangeredStatus: "least concern",
    regions: [
      "Upper Colorado Basin",
      "Lower Colorado Basin",
      "Arkansas-Rio Grande-Texas-Gulf",
    ],
    description:
      "The American black bear (Ursus americanus), also known as the black bear or sometimes baribal, is a medium-sized bear endemic to North America. It is the continent's smallest and most widely distributed bear species. American black bears are omnivores, with their diets varying greatly depending on season and location. They typically live in largely forested areas, but will leave forests in search of food, and are sometimes attracted to human communities due to the immediate availability of food.",
    image:
      "https://upload.wikimedia.org/wikipedia/commons/0/08/01_Schwarzb%C3%A4r.jpg",
    image2:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/Grand_Tetons_black_bear.jpg/440px-Grand_Tetons_black_bear.jpg",
  },
  {
    commonName: "Cougar",
    urlname: "Cougar",
    class: "Mammalia",
    genusSpecies: "Puma concolor",
    parks: ["Big Bend", "Grand Canyon", "White Sands"],
    states: ["Texas", "Arizona", "New Mexico"],
    endangeredStatus: "least concern",
    regions: [
      "Upper Colorado Basin",
      "Lower Colorado Basin",
      "Arkansas-Rio Grande-Texas-Gulf",
    ],
    description:
      "The cougar (Puma concolor) (/ˈkuːˌɡər/, KOO-gər) is a large cat native to the Americas. Its range spans from the Canadian Yukon to the southern Andes in South America and is the most widespread of any large wild terrestrial mammal in the Western Hemisphere. It is an adaptable, generalist species, occurring in most American habitat types. This wide range has brought it many common names, including puma, mountain lion, catamount and panther (for the Florida sub-population). It is the second-largest cat in the New World, after the jaguar (Panthera onca). Secretive and largely solitary by nature, the cougar is properly considered both nocturnal and crepuscular, although daytime sightings do occur. Despite its size, the cougar is more closely related to smaller felines, including the domestic cat (Felis catus) than to any species of the subfamily Pantherinae.",
    image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Mountain_Lion_in_Glacier_National_Park.jpg/440px-Mountain_Lion_in_Glacier_National_Park.jpg",
    image2:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/11/Torres_del_Paine_puma_JF2.jpg/440px-Torres_del_Paine_puma_JF2.jpg",
  },
];

export const getAnimal = (commonName) => {
  for (let i = 0; i < AnimalsList.length; i++) {
    if (commonName === AnimalsList[i].commonName) {
      return AnimalsList[i];
    }
  }
  return null;
};
