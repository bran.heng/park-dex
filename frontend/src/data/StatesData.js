const statesData = [
  {
    id: "nm",
    state: "New Mexico",
    img: "https://www.krqe.com/wp-content/uploads/sites/12/2020/07/AdobeStock-Welcome-to-New-Mexico.jpeg?w=876&h=493&crop=1",
    region: "Upper Colorado Basin",
    animals: "porcupine, coyote, cougar",
    numParks: 2,
    nationalParks: "Carlsbad Canverns, White Sands",
    area: "121,591",
    waterArea: "292",
    description:
      "New Mexico is the fifth-largest of the fifty states, but with just over 2.1 million residents, ranks 36th in population and 46th in population density. Its climate and geography are highly varied, ranging from forested mountains to sparse deserts; the northern and eastern regions exhibit a colder alpine climate, while the west and south are warmer and more arid. The Rio Grande and its fertile valley runs from north-to-south, creating a riparian climate through the center of the state that supports a bosque habitat and distinct Albuquerque Basin climate. One–third of New Mexico's land is federally owned, and the state hosts many protected wilderness areas and national monuments, including three UNESCO World Heritage Sites, the most of any state.",
    map: "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fe/New_Mexico_in_United_States.svg/600px-New_Mexico_in_United_States.svg.png",
  },
  {
    id: "tx",
    state: "Texas",
    img: "https://thumbnails.texastribune.org/HC4iZo8yPsbVf_ud-DGSm_ntcns=/850x570/smart/filters:quality(75)/https://static.texastribune.org/media/files/216c8fedb9f37b011cc98780a849aa0a/Welcome_to_Texas_Famartin_Wiki.jpg",
    region: "Arkansas-Rio Grande-Texas-Gulf",
    animals: "american black bear, elk, bighorn sheep",
    numParks: 2,
    nationalParks: "Big Bend, Guadelupe Mountains",
    area: "268,596",
    waterArea: "7,365",
    description:
      "Due to its size and geologic features such as the Balcones Fault, Texas contains diverse landscapes common to both the U.S. Southern and the Southwestern regions. Although Texas is popularly associated with the U.S. southwestern deserts, less than ten percent of Texas's land area is desert. Most of the population centers are in areas of former prairies, grasslands, forests, and the coastline. Traveling from east to west, one can observe terrain that ranges from coastal swamps and piney woods, to rolling plains and rugged hills, and finally the desert and mountains of the Big Bend.",
    map: "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ad/Texas_in_United_States.svg/600px-Texas_in_United_States.svg.png",
  },
  {
    id: "az",
    state: "Arizona",
    img: "https://image.cnbcfm.com/api/v1/image/107072948-1654710277640-gettyimages-518727666-000090196833_Unapproved.jpeg?v=1654710301",
    region: "Lower Colorado Basin",
    animals: "bison, coyote, cougar",
    numParks: 3,
    nationalParks: "Grand Canyon, Petrified Forest, Saguaro",
    area: "113,990",
    waterArea: "341",
    description:
      "Southern Arizona is known for its desert climate, with very hot summers and mild winters. Northern Arizona features forests of pine, Douglas fir, and spruce trees; the Colorado Plateau; mountain ranges (such as the San Francisco Mountains); as well as large, deep canyons, with much more moderate summer temperatures and significant winter snowfalls. There are ski resorts in the areas of Flagstaff, Alpine, and Tucson. In addition to the internationally known Grand Canyon National Park, which is one of the world's seven natural wonders, there are several national forests, national parks, and national monuments.",
    map: "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d8/Arizona_in_United_States.svg/600px-Arizona_in_United_States.svg.png",
  },
];

export function getStatesData() {
  return statesData;
}

export function getSingleState(stateId) {
  for (let i = 0; i < statesData.length; i++) {
    if (statesData[i].id === stateId) {
      return statesData[i];
    }
  }
  return null; // if stateId is not found in the array
}
