/**
 * @jest-environment jsdom
 */

import React from "react";

import renderer from "react-test-renderer";
import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

import {BrowserRouter as Router, Route, Routes} from "react-router-dom"
import AnimalCard from "../components/Animals/AnimalCard.js"
import StateCard from "../components/States/StateCard.js"
import ParkCard from "../components/Parks/ParkCard.js"
import ParkDetails from "../components/Parks/ParkDetails.js"
import AnimalDetails from "../components/Animals/AnimalDetails.js"
import StateDetails from "../components/States/StateDetails.js"
import ToolDetail from "../components/ToolDetail.js"
import TeamMemberCard from "../components/TeamMemberCard.js"
import Navbar from "../components/Navbar.js"
import park_test_json from "./park_test.json"
import animal_test_json from "./animal_test.json"
import state_test_json from "./state_test.json"

// Copied from https://gitlab.com/sarthaksirotiya/cs373-idb/-/blob/main/front-end/src/__tests__/test.js

const mockPerformanceMark = jest.fn();
window.performance.mark = mockPerformanceMark;

test("test whether the animal card works", () => {
  const animal = animal_test_json;
  const tree = renderer
    .create(
      <Router>
        <AnimalCard animal={animal} highlight={""} />
      </Router>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});

test("test whether the state card works", () => {
  const state = state_test_json;
  const tree = renderer
    .create(
      <Router>
        <StateCard data={state} highlight={""} />
      </Router>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});

test("test whether the park card works", () => {
  const park = park_test_json;
  const tree = renderer
    .create(
      <Router>
        <ParkCard park={park} highlight={""} />
      </Router>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});

test("test whether the animal details works", () => {
  const animal = animal_test_json;
  const tree = renderer
    .create(
      <Router>
        <AnimalDetails animal={animal} />
      </Router>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});

test("test whether the state details works", () => {
  const state = state_test_json;
  const tree = renderer
    .create(
      <Router>
        <StateDetails state={state} />
      </Router>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});

test("test whether the park details works", () => {
  const park = park_test_json;
  const tree = renderer
    .create(
      <Router>
        <ParkDetails park={park} showLatLong={false} />
      </Router>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});

test("Test whether the tools detail works", () => {
  const tool = {
    name: "React.js",
    icon: "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/2300px-React-icon.svg.png",
    link: "https://reactjs.org/",
  };
  const tree = renderer
    .create(
      <ToolDetail
        toolLink={tool.link}
        toolName={tool.name}
        toolIcon={tool.icon}
      />
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});

test("Test whether the card showing each team member's details works", () => {
  const teamMember = {
    fullName: "Chaitanya Eranki",
    headshot:
      "https://miro.medium.com/v2/resize:fit:720/format:webp/1*Y48-1i1oN6US7iJrI8_uZQ.jpeg",
    gitlabUsername: "Chai77",
    email: ["chaitanyaeranki@y7mail.com", "eranki.srinivasa@gmail.com"],
    role: "Frontend",
    unitTests: 0,
    bio: "I am a third-year CS major at the University of Texas at Austin. I enjoy playing chess and watching movies.",
  };
  const tree = renderer.create(
    <TeamMemberCard
      fullName={teamMember.fullName}
      img={teamMember.headshot}
      gitlabUsername={teamMember.gitlabUsername}
      email={teamMember.email[0]}
      role={teamMember.role}
      bio={teamMember.bio}
      numCommits={24}
      numIssues={10}
      numUserTests={8}
    />
  );
  expect(tree).toMatchSnapshot();
});

test("Test whether the Navbar works", () => {
  const tree = renderer.create(
    <Router>
      <Navbar />
    </Router>
  );
  expect(tree).toMatchSnapshot();
});

test("Test whether the Navbar contains links to About, Home, Parks, Animals, and States", async () => {
  render(
    <Router>
      <Navbar />
    </Router>
  );
  const navbar = screen.getByTestId("navbar");
  expect(navbar).toHaveTextContent("About");
  expect(navbar).toHaveTextContent("Home");
  expect(navbar).toHaveTextContent("Parks");
  expect(navbar).toHaveTextContent("Animals");
  expect(navbar).toHaveTextContent("States");
});
