from flask_marshmallow import Marshmallow
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema
from models import Animal, Park, State

ma = Marshmallow()


class AnimalSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Animal


class ParkSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Park


class StateSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = State


animal_schema = AnimalSchema()
park_schema = ParkSchema()
state_schema = StateSchema()
