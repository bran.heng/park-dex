import json
from models import Animal, State, Park, app, db
from schema import park_schema, animal_schema, state_schema
from flask_cors import CORS

CORS(app)

# Referenced Geo-Jobs (2022)
def create_url_from_scientific_name(scientific_name):
    return scientific_name.replace(" ", "_").lower()

#populate parks table
def populate_parks():
    with open("./data_scripts/parks/parks_final_data_states.json") as jsn:
        parks = json.load(jsn)
        parks_data = parks["Parks"]
        for id, park in enumerate(parks_data):
            db_row = {
                "id": id + 1,
                "full_name":        park["fullName"],
                "parkCode":         park["parkCode"],
                "url":              park["url"],
                "latLong":          park["latLong"],
                "description":      park["description"],
                "activities":       park["activities"],
                "phoneNumbers":     park["phoneNumbers"],
                "image1Url":        park["imageUrl"],
                "weatherInfo":      park["weatherInfo"],
                "directionInfo":    park["directionInfo"],
                "entranceFees":     park["entranceFee"],
                "image2Url":        park["image"],
                "animals_in_park":  [],
                "states_in_park":   [],
            }
            for animal_scientific_name in park["animals"]:
                # Find animal with this scientific name
                animal = Animal.query.filter_by(
                    id=create_url_from_scientific_name(animal_scientific_name)
                ).first()
                if animal == None:
                    continue
                db_row["animals_in_park"].append(animal)
            
            for state in park["states"]:
                # Find state with this state abbreviation
                state = State.query.filter_by(id=state.lower()).first()
                db_row["states_in_park"].append(state)
            
            park_data = Park(**db_row)
            db.session.add(park_data)

        db.session.commit()

#populate animals table
def populate_animals():
    with open("./data_scripts/animals/animals_final_sql_format.json") as jsn:
        animals = json.load(jsn)
        animals_data = animals["animals"]

        states_list = State.query.all()
        states_dicts = state_schema.dump(states_list, many=True)
        states_dict = {}
        for i in range(len(states_list)):
            # find state with this state abbreviation
            states_dict[states_dicts[i]["abbreviation"]] = states_list[i]

        for id, animal in enumerate(animals_data):
            if "description_accurate" in animal:
                something = animal["description_accurate"]
            else:
                something = False

            # split the states into a list
            state_list = animal["states"].split(",")
            db_row = {
                "id": create_url_from_scientific_name(animal["scientific_name"]),
                "scientific_name":      animal["scientific_name"],
                "common_name":          animal["common_name"],
                "endangered_status":    animal["endangered_status"],
                "species_group":        animal["species_group"],
                "foreign_species":      animal["foreign_species"],
                "taxonomic_family":     animal["taxonomic_family"],
                "taxonomic_kingdom":    animal["taxonomic_kingdom"],
                "states":               state_list,
                "description":          animal["description"],
                "wikipedia_url":        animal["wikipedia_url"],
                "wikipedia_page_name":  animal["wikipedia_page_name"],
                "description_accurate": something,
                "image1url":            animal["image1"],
                "image2url":            animal["image2"],
                "animals_in_state":     [],
            }

            for state in state_list:
                if state not in states_dict:
                    continue
                state = states_dict[state]
                db_row["animals_in_state"].append(state)

            if len(db_row["animals_in_state"]) == 0:
                continue

            db.session.add(Animal(**db_row))

        db.session.commit()

#populate states table
def populate_states():
    with open("./data_scripts/states/states.json") as jsn:
        states = json.load(jsn)
        states_data = states["states"]
        for id, state in enumerate(states_data):
            db_row = {
                "id":           state["abbreviation"].lower(),
                "name":         state["name"],
                "abbreviation": state["abbreviation"],
                "landArea":     state["land_area"],
                "waterArea":    state["water_area"],
                "description":  state["description"],
                "image":        state["image"],
                "region":       state["region"],
            }
            db.session.add(State(**db_row))
        db.session.commit()


def reset_db():
    db.session.remove()
    db.drop_all()
    db.create_all()


def init_db():
    print("Resetting DB")
    reset_db()
    print("Populating States")
    populate_states()
    print("Populating Animals")
    populate_animals()
    print("Populating Parks")
    populate_parks()
    print("Done!")


if __name__ == "__main__":
    with app.app_context():
        init_db()