from flask import Flask, request, jsonify, Response
from models import Animal, State, Park, app, db
from schema import AnimalSchema, ParkSchema, StateSchema
from flask_cors import CORS
import json
from sqlalchemy import or_, func, and_, Integer
from sqlalchemy.sql.expression import asc, desc


# get activities from json file using get_activities.py
# from data_scripts.parks.get_activities import activities_dict

# Park-dex (2022)
# A web application for searching parks, animals, and states.

# Default page size for pagination
DEFAULT_PAGE_SIZE = 25

# Dictionary for mapping activity tags to their IDs
activity_tags = {
    'Astronomy': 1, 
    'Stargazing': 2, 
    'Biking': 3, 
    'Camping': 4, 
    'Group Camping': 5, 
    'Fishing': 6, 
    'Food': 7, 
    'Guided Tours': 8, 
    'Hiking': 9, 
    'Front-Country Hiking': 10, 
    'Horse Trekking': 11, 
    'Paddling': 12, 
    'Junior Ranger Program': 13, 
    'Wildlife Watching': 14, 
    'Birdwatching': 15, 
    'Park Film': 16, 
    'Shopping': 17, 
    'Bookstore and Park Store': 18, 
    'Backcountry Camping': 19, 
    'Car or Front Country Camping': 20, 
    'Backcountry Hiking': 21, 
    'Canoe or Kayak Camping': 22, 
    'Museum Exhibits': 23
}

# Define the home route
@app.route("/")
def home():
    return "Welcome to Park-dex"

# Search for animals using the provided search terms
def search_animals(search_terms):
    query = db.session.query(Animal)
    or_queries = [Animal.common_name.ilike(f"%{term}%") for term in search_terms]
    or_queries += [Animal.scientific_name.ilike(f"%{term}%") for term in search_terms]
    query = query.filter(or_(*or_queries))
    return query

# Search for parks using the provided search terms
def search_parks(search_terms):
    query = db.session.query(Park)
    or_queries = [Park.full_name.ilike(f"%{term}%") for term in search_terms]
    or_queries += [Park.parkCode.ilike(f"%{term}%") for term in search_terms]
    query = query.filter(or_(*or_queries))
    return query

# Search for states using the provided search terms
def search_states(search_terms):
    query = db.session.query(State)
    or_queries = [State.name.ilike(f"%{term}%") for term in search_terms]
    # Append state abbreviation to search terms
    or_queries += [State.id.ilike(f"%{term}%") for term in search_terms]

    query = query.filter(or_(*or_queries))
    return query

# Format the results of an animal query
def format_animals(animal_query):
    animal_schema = AnimalSchema()
    result = []
    for animal in animal_query:
        animal_dict = animal_schema.dump(animal)
        animal_states = []
        animal_parks = []
        for state in animal.animals_in_state:
            animal_states.append(
                {
                    "name": state.name,
                    "abbr": state.abbreviation,
                }
            )
        for park in animal.parks_with_animal:
            animal_parks.append(
                {
                    "name": park.full_name,
                    "parkCode": park.parkCode,
                    "id": park.id,
                }
            )
        animal_dict["states_relate"] = animal_states
        animal_dict["parks_relate"] = animal_parks
        result.append(animal_dict)
    return result

# Format the results of a park query

def format_parks(park_query):
    park_schema = ParkSchema()
    result = []
    for park in park_query:
        park_dict = park_schema.dump(park)
        park_states = []
        park_animals = []
        for state in park.states_in_park:
            park_states.append(
                {
                    "name": state.name,
                    "abbr": state.abbreviation,
                    "region": state.region,
                }
            )
        for animal in park.animals_in_park:
            park_animals.append(
                {
                    "name": animal.common_name,
                    "scientific_name": animal.scientific_name,
                    "id": animal.id,
                }
            )
        park_dict["states_relate"] = park_states
        park_dict["animals_relate"] = park_animals
        result.append(park_dict)
    return result

# Format the results of a state query
def format_states(state_query):
    state_schema = StateSchema()
    result = state_schema.dump(state_query, many=True)

    final_result = []
    for i in range(len(result)):
        updated_result = dict(result[i])
        updated_result["parks"] = []
        for park in state_query[i].parks_with_state:
            updated_result["parks"].append(
                {
                    "id": park.id,
                    "park_name": park.full_name,
                }
            )
        updated_result["animals"] = []
        for animal in state_query[i].states_with_animal:
            updated_result["animals"].append(
                {
                    "id": animal.id,
                    "common_name": animal.common_name,
                    "scientific_name": animal.scientific_name,
                }
            )
        final_result.append(updated_result)
    return final_result

# Search for all models using the provided search terms
def get_search_terms(search):
    return search.split()

# Search for all models using the provided search terms
def search_all(search_terms):
    animal_query = search_animals(search_terms)
    park_query = search_parks(search_terms)
    state_query = search_states(search_terms)

    return {
        "animals": animal_query.all(),
        "parks": park_query.all(),
        "states": state_query.all(),
    }


# Search for all models using the provided search terms
def search_models(search_terms, model):
    if model == "animals":
        return search_animals(search_terms).all()
    elif model == "parks":
        return search_parks(search_terms).all()
    elif model == "states":
        return search_states(search_terms).all()
    else:
        return None
    
    # Format the results of a search query
def format_results(results, model):
    if model == "animals":
        return format_animals(results)
    elif model == "parks":
        return format_parks(results)
    elif model == "states":
        return format_states(results)
    else:
        return None

# Searches all models
@app.route("/api/search")
def search():
    search_query = request.args.get("search", type=str, default=None)
    model = request.args.get("model", type=str, default=None)

    if not search_query:
        return jsonify({"error": "Search query is required"}), 400

    search_terms = get_search_terms(search_query)

    if model:
        results = search_models(search_terms, model)
        if results is None:
            return jsonify({"error": "Invalid model"}), 400
        else:
            formatted_results = format_results(results, model)
            return jsonify({"data": formatted_results})
    else:
        results = search_all(search_terms)
        formatted_results = {
            "animals": format_results(results["animals"], "animals"),
            "parks": format_results(results["parks"], "parks"),
            "states": format_results(results["states"], "states"),
        }
    return jsonify({"data": formatted_results})


# Get all animals
@app.route("/api/animals")
def get_animals():
    # Pagination

    page = request.args.get("page", type=int)
    if page == None:
        page = 1

    per_page = request.args.get("per_page", type=int)

    # Searches animals with the given terms, if given
    search = request.args.get("search", type=str, default=None)
    search_terms = get_search_terms(search) if search else []

    animal_schema = AnimalSchema()
    query = search_animals(search_terms)

    # Filtering
    group = request.args.get("group")
    states = request.args.get("states")
    endangered_status = request.args.get("endangered_status")

    if group:
        query = query.filter(Animal.species_group == group)
    if states:
         query = query.filter(Animal.states.contains(states))
    if endangered_status:
        query = query.filter(Animal.endangered_status == endangered_status)

    # Sorting
    # Add ascending and descending
    sort_by = request.args.get("sort_by", type=str, default=None)
    sort_order = request.args.get("sort_order", type=str, default="asc")

    if sort_by == "name":
        if sort_order == "asc":
            query = query.order_by(asc(Animal.common_name))
        elif sort_order == "desc":
            query = query.order_by(desc(Animal.common_name))
    elif sort_by == "scientific_name":
        if sort_order == "asc":
            query = query.order_by(asc(Animal.scientific_name))
        elif sort_order == "desc":
            query = query.order_by(desc(Animal.scientific_name))
    count = query.count()

    if per_page:
        query = query.paginate(
            page=page, per_page=per_page, error_out=False
        ).items
        count = len(query)

    result = []
    for animal in query:
        animal_dict = animal_schema.dump(animal)
        animal_states = []
        animal_parks = []
        for state in animal.animals_in_state:
            animal_states.append(
                {
                    "name": state.name,
                    "abbr": state.abbreviation,
                }
            )
        for park in animal.parks_with_animal:
            animal_parks.append(
                {
                    "name": park.full_name,
                    "parkCode": park.parkCode,
                    "id": park.id,
                }
            )
        animal_dict["states_relate"] = animal_states
        animal_dict["parks_relate"] = animal_parks
        result.append(animal_dict)

    response = jsonify({"count": count, "data": result})
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response


# Get all animals
@app.route("/api/animals-length")
def get_animals_length():
    query = db.session.query(Animal)
    count = query.count()
    return jsonify({"count": count})

# get animal by id
@app.route("/api/animals/<animal_id>")
def get_animals_id(animal_id):
    animal = db.session.query(Animal).filter_by(id=animal_id).first()
    if not animal:
        return Response(json.dumps({"error": f"{animal_id} not found"}))

    animal_schema = AnimalSchema()
    states_list = []
    parks_list = []

    for state in animal.animals_in_state:
        states_list.append(
            {
                "name": state.name,
                "id": state.abbreviation,
                "image": state.image
            }
        )

    for park in animal.parks_with_animal:
        parks_list.append(
            {
                "name": park.full_name,
                "parkCode": park.parkCode,
                "id": park.id,
                "image": park.image1Url
            }
        )

    result = animal_schema.dump(animal)
    result["states_relate"] = states_list
    result["parks_relate"] = parks_list

    response = jsonify({"data": result})
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response

# Get all parks
@app.route("/api/parks")
def get_parks():
    # Pagination
    page = request.args.get("page", type=int)
    if page == None:
        page = 1

    per_page = request.args.get("per_page", type=int)

    # Searches parks with the given terms, if given
    search = request.args.get("search", type=str, default=None)
    search_terms = get_search_terms(search) if search else []

    park_schema = ParkSchema()
    query = search_parks(search_terms)

    # Filtering
    states = request.args.get("states", type=str, default=None)
    if states:
        query = query.filter(Park.states_in_park.any(State.abbreviation == states))

    activity_filter = request.args.get("activities", type=str, default=None)
    if activity_filter:
        activity_list = activity_filter.split('&')
        activity_ids = [activity_tags.get(activity) for activity in activity_list if activity in activity_tags]
        if activity_ids:
            query = query.filter(Park.activities.any(func.jsonb_array_elements(Park.activities).cast(Integer).in_(activity_ids)))

    # Filter by region as defined in state model
    region = request.args.get("region", type=str, default=None)
    if region:
        query = query.filter(Park.states_in_park.any(State.region == region))

    # Sorting
    sort_by = request.args.get("sort_by", type=str, default=None)
    sort_order = request.args.get("sort_order", type=str, default="asc")

    if sort_by == "name":
        if sort_order == "asc":
            query = query.order_by(asc(Park.full_name))
        elif sort_order == "desc":
            query = query.order_by(desc(Park.full_name))
    elif sort_by == "biodiversity":
        if sort_order == "asc":
            query = query.outerjoin(Park.animals_in_park).group_by(Park.id).order_by(func.count(Animal.id).asc())
        elif sort_order == "desc":
            query = query.outerjoin(Park.animals_in_park).group_by(Park.id).order_by(func.count(Animal.id).desc())
    elif sort_by == "num_activities":
        if sort_order == "asc":
            query = query.order_by(asc(func.jsonb_array_length(Park.activities)))
        elif sort_order == "desc":
            query = query.order_by(desc(func.jsonb_array_length(Park.activities)))

    count = query.count()

    if per_page:
        query = query.paginate(
            page=page, per_page=per_page, error_out=False
        ).items
        count = len(query)

    result = []
    for park in query:
        park_dict = park_schema.dump(park)
        park_states = []
        park_animals = []
        for state in park.states_in_park:
            park_states.append(
                {
                    "name": state.name,
                    "abbr": state.abbreviation,
                    "region": state.region,
                }
            )
        for animal in park.animals_in_park:
            park_animals.append(
                {
                    "name": animal.common_name,
                    "scientific_name": animal.scientific_name,
                    "id": animal.id,
                }
            )
        park_dict["states_relate"] = park_states
        park_dict["animals_relate"] = park_animals
        result.append(park_dict)

    response = jsonify({"count": count, "data": result})
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response


# Get all parks
@app.route("/api/parks-length")
def get_parks_length():
    query = db.session.query(Park)
    count = query.count()
    return jsonify({"count": count})

# Get a specific park
@app.route("/api/parks/<park_id>")
def get_parks_id(park_id):
    park = db.session.query(Park).filter_by(id=park_id).first()
    if not park:
        return Response(json.dumps({"error": f"{park_id} not found"}))
    park_schema = ParkSchema()
    park_dict = park_schema.dump(park)
    park_states = []
    park_animals = []
    for state in park.states_in_park:
        park_states.append(
            {
                "name": state.name,
                "id": state.abbreviation,
                "image": state.image
            }
        )
    for animal in park.animals_in_park:
        park_animals.append(
            {
                "name": animal.common_name,
                "scientific_name": animal.scientific_name,
                "id": animal.id,
                "image": animal.image1url
            }
        )
    park_dict["states_relate"] = park_states
    park_dict["animals_relate"] = park_animals
    response = jsonify({"data": park_dict})
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response

# Get all states
@app.route("/api/states")
def get_states():
    # do pagination here
    page = request.args.get("page", type=int)
    if page == None:
        page = 1

    per_page = request.args.get("per_page", type=int)

    # Searches states with the given terms, if given
    search = request.args.get("search", type=str, default=None)
    search_terms = get_search_terms(search) if search else []

    state_schema = StateSchema()
    query = search_states(search_terms)

    # Sorting
    sort_by = request.args.get("sort_by", type=str, default=None)
    sort_order = request.args.get("sort_order", type=str, default="asc")
    if sort_by == "name":
        if sort_order == "asc":
            query = query.order_by(State.name.asc())
        else:
            query = query.order_by(State.name.desc())
    elif sort_by == "land_area":
        if sort_order == "asc":
            query = query.order_by(State.landArea.asc())
        else:
            query = query.order_by(State.landArea.desc())
    elif sort_by == "water_area":
        if sort_order == "asc":
            query = query.order_by(State.waterArea.asc())
        else:
            query = query.order_by(State.waterArea.desc())
    elif sort_by == "biodiversity":
        if sort_order == "asc":
            query = query.outerjoin(Animal.animals_in_state).group_by(State.id).order_by(func.count(Animal.id).asc())
        else:
            query = query.outerjoin(Animal.animals_in_state).group_by(State.id).order_by(func.count(Animal.id).desc())
    elif sort_by == "number_of_parks":
        if sort_order == "asc":
            query = query.outerjoin(Park.states_in_park).group_by(State.id).order_by(func.count(Park.id).asc())
        else:
            query = query.outerjoin(Park.states_in_park).group_by(State.id).order_by(func.count(Park.id).desc())

    count = query.count()

    if per_page:
        query = query.paginate(
            page=page, per_page=per_page, error_out=False
        ).items
        count = len(query)

    result = state_schema.dump(query, many=True)

    final_result = []
    for i in range(len(result)):
        updated_result = dict(result[i])
        updated_result["parks"] = []
        for park in query[i].parks_with_state:
            updated_result["parks"].append(
                {
                    "id": park.id,
                    "park_name": park.full_name,
                    "image": park.image1Url
                }
            )
        updated_result["animals"] = []
        for animal in query[i].states_with_animal:
            updated_result["animals"].append(
                {
                    "id": animal.id,
                    "common_name": animal.common_name,
                    "scientific_name": animal.scientific_name,
                    "image": animal.image1url
                }
            )
        final_result.append(updated_result)

    response = jsonify({"count": count, "data": final_result})
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response

# Get all states length
@app.route("/api/states-length")
def get_states_length():
    query = db.session.query(State)
    count = query.count()
    return jsonify({"count": count})

# Get a specific state
@app.route("/api/states/<state_id>")
def get_states_id(state_id):
    state = db.session.query(State).filter_by(id=state_id).first()
    if not state:
        return Response(json.dumps({"error": f"{state_id} not found"}))
    state_schema = StateSchema()
    result = state_schema.dump(state, many=False)

    result["parks"] = []
    for park in state.parks_with_state:
        result["parks"].append(
            {
                "id": park.id,
                "name": park.full_name,
                "image": park.image1Url
            }
        )
    result["animals"] = []
    for animal in state.states_with_animal:
        result["animals"].append(
            {
                "id": animal.id,
                "name": animal.common_name,
                "scientific_name": animal.scientific_name,
                "image": animal.image1url
            }
        )

    response = jsonify({"data": result})
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response



if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)
