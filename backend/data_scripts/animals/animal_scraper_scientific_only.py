"""
https://ecos.fws.gov/ecp/report/adhocCreator?catalogId=species&reportId=species&columns=%2Fspecies@sn,cn,status,gn,is_foreign;%2Fspecies%2Ftaxonomy@family,kingdom;%2Fspecies%2FspeciesImage@image_url;%2Fspecies%2Frange_combined_states@all_states&sort=%2Fspecies@sn%20asc
"""

import requests
import wikipedia
import json

SKIP_PLANTS = True


def get_ecos_images(scientific_names_dict):
    response = requests.get(
        "https://ecos.fws.gov/ecp/pullreports/catalog/species/report/species/export?format=json&columns=%2Fspecies%40sn%3B%2Fspecies%2FspeciesImage%40image_url&sort=%2Fspecies%40sn%20asc"
    )
    data = response.json()
    for animal in data["data"]:
        scientific_name = animal[0]["value"]
        image_url = animal[1]["url"]
        if scientific_name in scientific_names_dict:
            scientific_names_dict[scientific_name]["images"].append(image_url)


def get_all_ecos_animals():
    scientific_names = {}
    animals_list = []
    response = requests.get(
        "https://ecos.fws.gov/ecp/pullreports/catalog/species/report/species/export?format=json&columns=%2Fspecies%40sn%2Ccn%2Cstatus%2Cgn%2Cis_foreign%3B%2Fspecies%2Ftaxonomy%40family%2Ckingdom%3B%2Fspecies%2Frange_combined_states%40all_states&sort=%2Fspecies%40sn%20asc"
    )
    data = response.json()
    for animal in data["data"]:
        scientific_name = animal[0]["value"]
        if scientific_name in scientific_names:
            continue

        if animal[7] == None:
            continue

        animal_info_dict = {}
        animal_info_dict["scientific_name"] = scientific_name
        animal_info_dict["common_name"] = animal[1]
        animal_info_dict["endangered_status"] = animal[2]
        animal_info_dict["species_group"] = animal[3]
        animal_info_dict["foreign_species"] = animal[4]
        animal_info_dict["taxonomic_family"] = animal[5]
        animal_info_dict["taxonomic_kingdom"] = animal[6]
        animal_info_dict["states"] = animal[7].split(", ")
        animal_info_dict["images"] = []
        animal_info_dict["description"] = None
        animal_info_dict["wikipedia_url"] = None
        animal_info_dict["wikipedia_page_name"] = None

        if SKIP_PLANTS and animal_info_dict["taxonomic_kingdom"] == "Plant":
            continue

        if animal_info_dict["common_name"] == "No common name":
            continue

        animals_list.append(animal_info_dict)
        scientific_names[scientific_name] = animal_info_dict

    return animals_list, scientific_names


def get_wikipedia_information_for_animal(animal_info):
    try:
        results_scientific = wikipedia.search(
            animal_info["scientific_name"], suggestion=False
        )
        if len(results_scientific) == 0:
            return None

        # get description of animal and put it in description
        if (results_scientific[0] != animal_info["scientific_name"]):
            # description may not be accurate because there is no exact match
            animal_info["description_accurate"] = False

        try:
            wikipedia_search_term = (
                results_scientific[0]
            )
            animal_page = wikipedia.page(wikipedia_search_term, auto_suggest=False)
        except wikipedia.DisambiguationError as e:
            return None
        animal_info["description"] = animal_page.summary.strip()
        animal_info["wikipedia_url"] = animal_page.url
        animal_info["wikipedia_page_name"] = wikipedia_search_term
        animal_info["images"] += animal_page.images
    except Exception as e:
        print(e)
        return None


all_animals, scientific_names_dict = get_all_ecos_animals()
get_ecos_images(scientific_names_dict)

num = 0
for animal_info in all_animals:
    get_wikipedia_information_for_animal(animal_info)

    num += 1
    print("Done", num)

json_object = json.dumps(all_animals, indent=4)
with open("animals.json", "w") as outfile:
    outfile.write(json_object)
