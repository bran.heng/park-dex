import json

with open("./animals_final.json") as f:
    vals = json.load(f)

new_json = []
for val in vals:
    val["image1"] = (
        val["images"][0] if val["images"] != None and len(val["images"]) >= 1 else None
    )
    val["image2"] = (
        val["images"][1] if val["images"] != None and len(val["images"]) >= 2 else None
    )
    del val["images"]
    val["states"] = ",".join(val["states"])
    new_json.append(val)

json_object = json.dumps(new_json, indent=4)
with open("animals_final_sql_format.json", "w") as outfile:
    outfile.write(json_object)
