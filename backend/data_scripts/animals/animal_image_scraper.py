import json
import requests
import urllib

with open("./animals.json") as f:
    animals = json.load(f)

changed_animals_list = []


print("Total Entries", len(animals))
entry_num = 0
for animal_info in animals:
    print("Current Animal:", entry_num)
    entry_num += 1

    if animal_info["wikipedia_url"] == None:
        changed_animals_list.append(animal_info)
        continue
    wikipedia_url = animal_info["wikipedia_url"]
    wikipedia_name = wikipedia_url.split("/")[-1]

    url_to_get = (
        "https://en.wikipedia.org/w/api.php?action=query&titles=%s&prop=pageimages&format=json&pithumbsize=100"
        % wikipedia_name
    )
    page_images = requests.get(url_to_get).json()

    pages_list = page_images["query"]["pages"]
    for val in pages_list.values():
        if "pageimage" in val:
            img_name_raw = val["pageimage"]
            img_name = urllib.parse.quote_plus(img_name_raw)

            # bring image source in list of images to top
            new_images_list = animal_info["images"]

            curr_index = -1
            for i in range(len(new_images_list)):
                if new_images_list[i].split("/")[-1] == img_name:
                    curr_index = i
                    break

            if curr_index != -1:
                insert_index = 0
                if new_images_list[0].startswith("https://ecos.fws.gov"):
                    insert_index = 1

                new_images_list.insert(insert_index, new_images_list.pop(curr_index))
                print(
                    "Moving %s from index %d to %d"
                    % (img_name, curr_index, insert_index)
                )
            else:
                print("The curr index was -1", new_images_list, ", ", img_name)

    changed_animals_list.append(animal_info)

json_object = json.dumps(changed_animals_list, indent=4)
with open("animals_final.json", "w") as outfile:
    outfile.write(json_object)
