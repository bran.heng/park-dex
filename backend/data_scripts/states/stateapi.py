import wikipedia
import requests
from bs4 import *
import json

from wikipedia.wikipedia import WikipediaPage


northAtlanticAppalachian = [
    "North Atlantic-Appalachian",
    "Connecticut",
    "Delaware",
    "Kentucky",
    "Maine",
    "Maryland",
    "Massachusetts",
    "New Hampshire",
    "New Jersey",
    "New York",
    "Pennsylvania",
    "Rhode Island",
    "Vermont",
    "Virginia",
    "West Virginia",
]
southAtlanticGulf = [
    "South Atlantic-Gulf",
    "Alabama",
    "Florida",
    "Georgia",
    "North Carolina",
    "Puerto Rico",
    "South Carolina",
    "Tennessee" "U.S. Virgin Islands",
]
greatLakes = [
    "Great Lakes",
    "Minnesota",
    "Wisconsin",
    "Michigan",
    "Illinois",
    "Indiana",
    "Ohio",
]
mississippiBasin = [
    "Mississippi Basin",
    "Arkansas",
    "Iowa",
    "Missouri",
    "Mississippi",
    "Louisiana",
]
missouriBasin = [
    "Missouri Basin",
    "Kansas",
    "Montana",
    "North Dakota",
    "Nebraska",
    "South Dakota",
]
arkansasRioGrandeTexasGulf = ["Arkansas-Rio Grande-Texas-Gulf", "Texas", "Oklahoma"]
upperColoradoBasin = [
    "Upper Colorado Basin",
    "Colorado",
    "Utah",
    "Wyoming",
    "New Mexico",
]
lowerColoradoBasin = [
    "Lower Colorado Basin",
    "Arizona",
    "Southern California",
    "Nevada",
]
columbiaPacificNorthwest = [
    "Columbia-Pacific Northwest",
    "Idaho",
    "Oregon",
    "Washington",
    "Montana",
]
californiaGreatBasin = ["California-Great Basin", "California", "Nevada", "Oregon"]
alaska = ["Alaska", "Alaska"]
pacificIslands = [
    "Pacific Islands",
    "American Samoa",
    "Hawaii",
    "Guam",
    "Commonwealth of the Northern Mariana Islands",
]

# [abbreviation, land area (miles^2), water area (miles^2),]
abbreviations = {
    "Alabama": ["AL", 50645, 1774],
    "Kentucky": ["KY", 39486, 921],
    "Ohio": ["OH", 40860, 3964],
    "Alaska": ["AK", 570640, 94743],
    "Louisiana": ["LA", 43203, 9174],
    "Oklahoma": ["OK", 68594, 1303],
    "Arizona": ["AZ", 113594, 396],
    "Maine": ["ME", 30842, 4536],
    "Oregon": ["OR", 95988, 2390],
    "Arkansas": ["AR", 52035, 1143],
    "Maryland": ["MD", 9707, 2698],
    "Pennsylvania": ["PA", 44742, 1311],
    "American Samoa": ["AS", 76, 504],
    "Massachusetts": ["MA", 7800, 2754],
    "Puerto Rico": ["PR", 3423, 1901],
    "California": ["CA", 155779, 7915],
    "Michigan": ["MI", 56538, 40174],
    "Rhode Island": ["RI", 1033, 511],
    "Colorado": ["CO", 103641, 451],
    "Minnesota": ["MN", 79626, 7309],
    "South Carolina": ["SC", 30060, 1959],
    "Connecticut": ["CT", 4842, 701],
    "Mississippi": ["MS", 46923, 1508],
    "South Dakota": ["SD", 75811, 1304],
    "Delaware": ["DE", 1948, 540],
    "Missouri": ["MO", 68741, 965],
    "Tennessee": ["TN", 41234, 909],
    "District of Columbia": ["DC", 61, 7],
    "Montana": ["MT", 145545, 1493],
    "Texas": ["TX", 261231, 7364],
    "Florida": ["FL", 53624, 12132],
    "Nebraska": ["NE", 76824, 523],
    "Georgia": ["GA", 57513, 1911],
    "Nevada": ["NV", 109781, 790],
    "Utah": ["UT", 82169, 2727],
    "Guam": ["GU", 209, 360],
    "New Hampshire": ["NH", 8952, 396],
    "Vermont": ["VT", 9216, 399],
    "Hawaii": ["HI", 6422, 4509],
    "New Jersey": ["NJ", 7354, 1368],
    "Virginia": ["VA", 39490, 3284],
    "Idaho": ["ID", 82643, 925],
    "New Mexico": ["NM", 121298, 292],
    "Virgin Islands": ["VI", 134, 598],
    "Illinois": ["IL", 55518, 2394],
    "New York": ["NY", 47126, 7428],
    "Washington": ["WA", 66455, 4842],
    "Indiana": ["IN", 35826, 593],
    "North Carolina": ["NC", 48617, 5201],
    "West Virginia": ["WV", 24038, 191],
    "Iowa": ["IA", 55857, 415],
    "North Dakota": ["ND", 69000, 1697],
    "Wisconsin": ["WI", 54157, 11338],
    "Kansas": ["KS", 81758, 519],
    "Northern Mariana Islands": ["MP", 182, 1793],
    "Wyoming": ["WY", 97093, 719],
}


def get_wikipedia_images(state):
    i = None
    try:
        i = wikipedia.WikipediaPage(state).images
    except wikipedia.DisambiguationError as e:
        for option in e.options:
            if "state" in option.lower():
                i = wikipedia.WikipediaPage(option).images
                break

    for j in range(2, len(i)):
        i.pop()
    return i


def get_wikipedia_summary(state):
    print(state)
    s = None
    try:
        s = wikipedia.summary(state, auto_suggest=False)
    except wikipedia.DisambiguationError as e:
        for option in e.options:
            if "state" in option.lower():
                s = wikipedia.summary(option, auto_suggest=False)
                break
        if s == None:
            print("Didn't find stuff with state")
            print(e.options)
    return s


def create_json():
    image_state_map = get_images_for_states()
    states_list = []
    for state in abbreviations.keys():
        vals = abbreviations[state]
        summary = get_wikipedia_summary(state)
        image = get_wikipedia_images(state)
        json_value = {
            "name": state,
            "abbreviation": vals[0],
            "land_area": vals[1],
            "water_area": vals[2],
            "image": image_state_map[state] if state in image_state_map else None,
            "description": summary,
        }
        states_list.append(json_value)
    json_object = json.dumps(states_list, indent=4)
    with open("states.json", "w") as outfile:
        outfile.write(json_object)


def get_images_for_states():
    r = requests.get(
        "https://www.thrillist.com/travel/nation/most-beautiful-states-in-america"
    )
    soup = BeautifulSoup(r.text, "html.parser")
    images = soup.find_all("img", {"class": "lazyload"})
    states = soup.find_all(
        "div", {"class": "ParagraphBodyTextstyles__BodyText-sc-zv221u-0"}
    )
    final_states = []
    final_images = []
    final_state_image_map = {}
    for state in states:
        vals = state.find_all("h2")
        if len(vals) == 0:
            continue
        state_text = vals[0].text

        final_states.append(state_text[state_text.index(" ") + 1 :])

    for image in images:
        final_images.append(image["data-src"])

    for i in range(len(final_images)):
        final_state_image_map[final_states[i]] = final_images[i]

    return final_state_image_map


create_json()
