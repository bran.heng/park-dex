import json

with open("./park_data.json") as f:
    original_data = json.load(f)

with open("./parks_final_data.json") as f:
    final_data = json.load(f)

states_dict = {}
output_data = {
    "Parks": [],
}

for park in original_data["data"]:
    states_dict[park["fullName"]] = park["states"].split(",")

for park in final_data:
    park["states"] = states_dict[park["fullName"]]
    output_data["Parks"].append(park)

json_object = json.dumps(output_data, indent=4)
with open("parks_final_data_states.json", "w") as outfile:
    outfile.write(json_object)
