import json
import wikipedia
import requests
import urllib

with open("./parks_data_formatted.json") as f:
    original_data = json.load(f)

output_parks = []

for park in original_data:
    images = []
    try:
        images = wikipedia.WikipediaPage(park["fullName"]).images
    except wikipedia.DisambiguationError as e:
        print(e.options)
    except wikipedia.exceptions.PageError as e:
        print("Could not find page")

    wikipedia_name = park["fullName"]
    url_to_get = (
        "https://en.wikipedia.org/w/api.php?action=query&titles=%s&prop=pageimages&format=json&pithumbsize=100"
        % wikipedia_name
    )
    page_images = requests.get(url_to_get).json()

    final_image = images[0] if len(images) > 0 else None
    pages_list = page_images["query"]["pages"]
    for val in pages_list.values():
        if "pageimage" in val:
            img_name_raw = val["pageimage"]
            img_name = urllib.parse.quote_plus(img_name_raw)

            curr_index = -1
            for i in range(len(images)):
                if images[i].split("/")[-1] == img_name:
                    curr_index = i
                    final_image = images[i]
                    break

    print(wikipedia_name, final_image)
    park["image"] = final_image

    output_parks.append(park)


json_object = json.dumps(output_parks, indent=4)
with open("parks_data_formatted_images.json", "w") as outfile:
    outfile.write(json_object)
