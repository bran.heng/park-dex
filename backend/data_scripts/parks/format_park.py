import json

with open("./park_data.json") as f:
    original_data = json.load(f)

output_data = []

for park in original_data["data"]:
    output_park = {
        "fullName": park["fullName"],
        "parkCode": park["parkCode"],
        "url": park["url"],
        "latLong": park["latLong"],
        "description": park["description"],
        "activities": [activity["name"] for activity in park["activities"]],
        "phoneNumbers": [
            phone["phoneNumber"] for phone in park["contacts"]["phoneNumbers"]
        ],
        "imageUrl": park["images"][0]["url"],
        "weatherInfo": park["weatherInfo"],
        "directionInfo": park["directionsInfo"],
        "entranceFee": [fee["cost"] for fee in park["entranceFees"]],
    }
    output_data.append(output_park)

json_object = json.dumps(output_data, indent=4)
with open("parks_data_formatted.json", "w") as outfile:
    outfile.write(json_object)
