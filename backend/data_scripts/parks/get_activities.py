import json

def get_activities_from_parks(json_file):
    with open(json_file, 'r') as file:
        data = json.load(file)

    activities_count = {}
    
    for park in data['Parks']:
        for activity in park.get('activities', []):
            if activity not in activities_count:
                activities_count[activity] = 0
            activities_count[activity] += 1

    # Filter activities appearing more than twice
    common_activities = {k: v for k, v in activities_count.items() if v > 30}

    # Assign incremental values to common activities
    common_activities = {activity: index + 1 for index, activity in enumerate(common_activities)}

    return common_activities

if __name__ == "__main__":
    json_file = 'backend/data_scripts/parks/parks_final_data_states.json'
    activities_dict = get_activities_from_parks(json_file)
    print(activities_dict)
