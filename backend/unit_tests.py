import unittest
import app

# Copied the code for tests from https://gitlab.com/sarthaksirotiya/cs373-idb/-/blob/main/back-end/tests.py
# Edited this code to work for our website

class Tests(unittest.TestCase):
    def setUp(self):
        # app.app.config["TESTING"] = True
        self.client = app.app.test_client()

    def testGetAllAnimalsPagination(self):
        with self.client:
            response = self.client.get("/api/animals?page=1&per_page=25")
            self.assertEqual(response.status_code, 200)
            data = response.json["data"]
            self.assertEqual(len(data), 25)

    def testGetAllStatesPagination(self):
        with self.client:
            response = self.client.get("/api/states?page=1&per_page=25")
            self.assertEqual(response.status_code, 200)
            data = response.json["data"]
            self.assertEqual(len(data), 25)

    def testGetAllParksPagination(self):
        with self.client:
            response = self.client.get("/api/parks?page=1&per_page=25")
            self.assertEqual(response.status_code, 200)
            data = response.json["data"]
            self.assertEqual(len(data), 25)

    def testGetAllAnimals(self):
        with self.client:
            response = self.client.get("/api/animals")
            self.assertEqual(response.status_code, 200)
            data = response.json["data"]
            self.assertEqual(len(data), 3038)

    def testGetAllStates(self):
        with self.client:
            response = self.client.get("/api/states")
            self.assertEqual(response.status_code, 200)
            data = response.json["data"]
            self.assertEqual(len(data), 56)

    def testGetAllParks(self):
        with self.client:
            response = self.client.get("/api/parks")
            self.assertEqual(response.status_code, 200)
            data = response.json["data"]
            self.assertEqual(len(data), 310)

    def testGetAnimalInstance(self):
        with self.client:
            response = self.client.get("/api/animals/acanthometropus_pecatonica")
            self.assertEqual(response.status_code, 200)
            resp = response.json
            data = resp["data"]
            self.assertEqual(data["common_name"], "Pecatonica River mayfly")
            self.assertEqual(data["species_group"], "Insects")
            self.assertEqual(len(data["states_relate"]), 2)

    def testGetStateInstance(self):
        with self.client:
            response = self.client.get("/api/states/ca")
            self.assertEqual(response.status_code, 200)
            resp = response.json
            data = resp["data"]
            self.assertEqual(len(data["parks"]), 23)
            self.assertEqual(data["name"], "California")
            self.assertEqual(data["landArea"], 155779)

    def testGetParkInstance(self):
        with self.client:
            response = self.client.get("/api/parks/3")
            self.assertEqual(response.status_code, 200)
            resp = response.json
            data = resp["data"]
            self.assertEqual(data["full_name"], "Agate Fossil Beds National Monument")
            self.assertEqual(data["parkCode"], "agfo")
            self.assertEqual(data["url"], "https://www.nps.gov/agfo/index.htm")

    def testParksLength(self):
        with self.client:
            response = self.client.get("/api/parks-length")
            self.assertEqual(response.status_code, 200)
            resp = response.json
            count = resp["count"]
            self.assertEqual(count, 310)

    def testAnimalsLength(self):
        with self.client:
            response = self.client.get("/api/animals-length")
            self.assertEqual(response.status_code, 200)
            resp = response.json
            count = resp["count"]
            self.assertEqual(count, 3038)

    def testStatesLength(self):
        with self.client:
            response = self.client.get("/api/states-length")
            self.assertEqual(response.status_code, 200)
            resp = response.json
            count = resp["count"]
            self.assertEqual(count, 56)

    def testSearch(self):
        with self.client:
            response = self.client.get("/api/search?search=oso")
            self.assertEqual(response.status_code, 200)
            resp = response.json
            animals = resp["data"]["animals"]
            states = resp["data"]["states"]
            parks = resp["data"]["parks"]
            self.assertEqual(type(animals), list)
            self.assertEqual(type(states), list)
            self.assertEqual(type(parks), list)


if __name__ == "__main__":
    unittest.main()
