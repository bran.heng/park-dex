from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from sqlalchemy.types import JSON


app = Flask(__name__)
CORS(app)
app.debug = True
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["SQLALCHEMY_DATABASE_URI"] = "mysql://admin:adminpassword@parkdexinstance.cpdkpokpabjk.us-east-2.rds.amazonaws.com:3306/parkdexDB"
db = SQLAlchemy(app)

# Association table to create many-to-many relationship between Parks and Animals
animal_parks = db.Table(
    "animal_parks",
    db.Column("animal_id", db.String(100), db.ForeignKey("animal.id")),
    db.Column("park_id", db.Integer, db.ForeignKey("park.id")),
)

# Association table to create many-to-many relationship between States and Animals
animal_states = db.Table(
    "animal_states",
    db.Column("animal_id", db.String(100), db.ForeignKey("animal.id")),
    db.Column("state_id", db.String(5), db.ForeignKey("state.id")),
)

# Association table to create many-to-many relationship between Parks and States
park_states = db.Table(
    "park_states",
    db.Column("park_id", db.Integer, db.ForeignKey("park.id")),
    db.Column("state_id", db.String(100), db.ForeignKey("state.id")),
    db.Column("region", db.String(100)),
)

#park model
class Park(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    full_name         = db.Column(db.String(100))
    parkCode          = db.Column(db.String(100))
    url               = db.Column(db.String(200))
    latLong           = db.Column(db.String(100))
    description       = db.Column(db.Text)
    activities        = db.Column(JSON)
    phoneNumbers      = db.Column(JSON)
    image1Url         = db.Column(db.String(200))
    weatherInfo       = db.Column(db.Text)
    directionInfo     = db.Column(db.Text)
    entranceFees      = db.Column(JSON)
    image2Url         = db.Column(db.String(200))
    animals_in_park   = db.relationship("Animal", secondary=animal_parks, backref="parks_with_animal")
    states_in_park    = db.relationship("State", secondary=park_states, backref="parks_with_state")

    def __repr__(self):
        return f"Park('{self.full_name}')"

#animal model
class Animal(db.Model):
    # num_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    id                    = db.Column(db.String(100), primary_key=True)
    scientific_name       = db.Column(db.String(100))
    common_name           = db.Column(db.String(100))
    endangered_status     = db.Column(db.String(100))
    species_group         = db.Column(db.String(100))
    foreign_species       = db.Column(db.Boolean)
    taxonomic_family      = db.Column(db.String(100))
    taxonomic_kingdom     = db.Column(db.String(100))
    states                = db.Column(JSON)
    description           = db.Column(db.Text)
    wikipedia_url         = db.Column(db.String(100))
    wikipedia_page_name   = db.Column(db.String(100))
    description_accurate  = db.Column(db.Boolean)
    image1url             = db.Column(db.String(200))
    image2url             = db.Column(db.String(200))
    park_ids              = db.Column(db.String(500))
    animals_in_state = db.relationship("State", secondary=animal_states, backref="states_with_animal")

    def __repr__(self):
        return f"<Animal {self.common_name}>"

#state model
class State(db.Model):
    id                = db.Column(db.String(5), primary_key=True)
    name              = db.Column(db.String(100))
    abbreviation      = db.Column(db.String(100))
    landArea          = db.Column(db.Integer)
    waterArea         = db.Column(db.Integer)
    image             = db.Column(db.Text)
    description       = db.Column(db.Text)
    region            = db.Column(db.String(100))

    def __repr__(self):
        return f"<State {self.name}>"