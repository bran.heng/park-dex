# park-dex

## Members
| Name               | EID     | GitLab ID        |
| ------------------ | ------- | ---------------- |
| Akash Aedavelli    | ara3793 | @akashra         |
| Alejandro Castillo | alc5938 | @alexcastillo333 |
| Chaitanya Eranki   | se22256 | @Chai77          |
| Branden Heng       | bh29935 | @bran.heng       |
| Dev Randalpura     | dmr3685 | @devr0306        |

## Git SHA
| Phase | SHA                                      |
| :---: | :--------------------------------------: |
| 1     | a3ee95c54e0cfaebbca182360f10b480210106be |
| 2     | d1328009bca6724f8d429e3a850dbe571a2044ef |
| 3     | a3fc2fd51a26504248ab01f8da4dfff90b1f3710 |
| 4     | 84180c8194f5a93979a612e33ae7948889667a78 |

## Project Leaders
| Phase | PL        |
| :---: | :-------: |
| 1     | Branden   |
| 2     | Chaitanya |
| 3     | Dev       |
| 4     | Akash     |

* Project Leader should split up tasks evenly
* Project Leader is in charge of keeping all on task
* Project Leader should check up on members, see if they're well
___
**GitLab Pipeline:** https://gitlab.com/bran.heng/park-dex/-/pipelines
---

**Website:** https://www.park-dex.me/
---

## Completion Time
### Phase 1
| Name               | Estimated     | Actual        |
| ------------------ | :-----------: | :-----------: |
| Akash Aedavelli    | 10            | 15            |
| Alejandro Castillo | 10            | 15            |
| Chaitanya Eranki   | 5             | 18            |
| Branden Heng       | 10            | 21            |
| Dev Randalpura     | 10            | 15            |

### Phase 2
| Name               | Estimated     | Actual        |
| ------------------ | :-----------: | :-----------: |
| Akash Aedavelli    | 25            | 44            |
| Alejandro Castillo | 20            | 32            |
| Chaitanya Eranki   | 15            | 40            |
| Branden Heng       | 20            | 36            |
| Dev Randalpura     | 20            | 25            |

### Phase 3
| Name               | Estimated     | Actual        |
| ------------------ | :-----------: | :-----------: |
| Akash Aedavelli    |  7            | 10            |
| Alejandro Castillo | 10            | 10            |
| Chaitanya Eranki   |  6            | 10            |
| Branden Heng       | 10            | 10            |
| Dev Randalpura     |  8            | 10            |

### Phase 4
| Name               | Estimated     | Actual        |
| ------------------ | :-----------: | :-----------: |
| Akash Aedavelli    | 10            |  8            |
| Alejandro Castillo | 10            |  6            |
| Chaitanya Eranki   |  5            |  8            |
| Branden Heng       | 20            |  6            |
| Dev Randalpura     | 10            |  6            |

## Comments
Referenced GeoJob's (Fall 2022) Dockerfile, makefile, app.py, db.py, .gitlab-ci.yml, unit_tests

Referenced MusicMap's db.py

Referenced Codemy.com's video https://youtu.be/Q2QmST-cSwc for SQLAlchemy

Referenced Cloud Guru's video https://www.youtube.com/watch?v=zOsO996Esck for AWS Database setup

___
# OLD INFO

**IDB Group 11**

**Group Members:** Akesh Aedavelli, Alejandro Castillo, Chaitanya Eranki, Branden Heng, Dev Randalpura

## Project Info

**Website Name:** park-dex.me

Parkdex is a website that will allow users to find detailed information about parks within their state, and inform users with extensive data about animals that live within a specified state or upon encounter, localized entirely within one website.

## APIs

- General information about national and state parks:
  - https://www.nps.gov/subjects/developer/api-documentation.htm#/parks/getPark
  - https://stateparks.com/index.html
- Specific information about animals and parks (descriptions):
  - https://www.mediawiki.org/wiki/API:REST_API
- Data about species (endangered status):
  - https://ecos.fws.gov/ecp/report/adhocDocumentation?catalogId=species&reportId=species
  - https://apiv3.iucnredlist.org/api/v3/docs
  - https://irmaservices.nps.gov/v3/rest/NPSpecies/help

## Models

### Parks

- **Estimated Instance Count:** ~400
- **Attributes for Filtering/Sorting:** type, national/state, state, region, animals
- **Media & additional Attributes:** name, image, address, description, state, contact information, map
- **Connection to other Models:** Parks are located within states, parks contain animals.

### Animals

- **Estimated Instance Count:** 1,000,000
- **Attributes for Filtering/Sorting:** animal family, state, region, species, endangered status
- **Media & additional Attributes:** name, scientific name, image, description, locations, endangered status, population
- **Connection to other Models:** Animals inhabit states and parks.

### States

- **Estimated Instance Count:** ~50
- **Attributes for Filtering/Sorting:** region, animals, parks, biodiversity, instances of parks
- **Media & additional Attributes:** name, image, map, description, state animal
- **Connection to other Models:** States have their own parks, including national parks, each with their own animals.

## Organization Technique

We will be utilizing Professor Downing's suggested method of one page per model with grid of card instances.

## Questions

- What parks and animals are located in my state?
- What information can I discover about animals I have encountered?
- Who's that animal (_upon encountering an animal_ )?
